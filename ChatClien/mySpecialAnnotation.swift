//
//  mySpecialAnnotation.swift
//  ChatClien
//
//  Created by Tudor on 14/08/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import MapKit

class mySpecialAnnotation:NSObject, MKAnnotation {

    var coordinate: CLLocationCoordinate2D
    var title: String
    var subtitle: String
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
    override init() {
        self.title = ""
        self.coordinate = CLLocationCoordinate2DMake(0, 0)
        self.subtitle = ""
    }
    
    func initWithTitle (newTitle : NSString ,Location location :CLLocationCoordinate2D){
 
       
            self.title = newTitle
            self.coordinate = location
        
        
        
        
    }
    func annotationView() -> MKAnnotationView {
        var annotationView : MKAnnotationView!
        annotationView = MKAnnotationView(annotation: self, reuseIdentifier: "MyCustomAnnotation")
        annotationView.enabled = true
        annotationView.canShowCallout = true
        annotationView.image = UIImage(named: "map6mod")
        annotationView.rightCalloutAccessoryView = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as UIView
        
        return annotationView
    }



}
