//
//  groupChatController.swift
//  ChatClien
//
//  Created by Tudor on 17/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit

class groupChatController: UIViewController,UIBubbleTableViewDataSource ,NSStreamDelegate,UITextViewDelegate{
 
    @IBOutlet var label: UILabel!
    @IBOutlet var ChatView: UIBubbleTableView!
    @IBOutlet var textInput: UITextView!
    @IBOutlet var image: UIImageView!
    
    @IBOutlet var sendButton: UIButton!
    
    @IBOutlet var groupName: UILabel!
    
    var bubbleData :NSMutableArray! = NSMutableArray()
    var name :NSString!
    var inputStream :NSInputStream!
    var outputStream :NSOutputStream!
    var group :NSString!
    var pos = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ChatView.bubbleDataSource = self
        ChatView.snapInterval = 120
        ChatView.showAvatars = true
        //ChatView.reloadData()
        
        self.textInput.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidShow:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidHide:", name:UIKeyboardDidHideNotification, object: nil)
       // ChatView.reloadData()
       // bubbleData.addObject("dsadsa")
        self.sendButton.layer.cornerRadius = 4
        self.label.text = name
        self.groupName.text = group
        // Do any additional setup after loading the view.
        self.initNetworkCommunication()
        //self.ChatView.scrollBubbleViewToBottomAnimated(true)
       
        
    }
    
    func keyboardDidShow(notification :NSNotification){
        //println("A aparut")
       // print("\(self.messageText.frame.origin)")
        //
        var c :NSDictionary = notification.userInfo! as NSDictionary
        var x = notification.userInfo
        
        var point:NSValue = NSValue(nonretainedObject: c.objectForKey(UIKeyboardFrameEndUserInfoKey))
        //var keyboardSize = let dict:NSDictionary = sender.userInfo as NSDictionary
        let s:NSValue = c.valueForKey(UIKeyboardFrameEndUserInfoKey) as NSValue;
        let rect :CGRect = s.CGRectValue();
        
        var buttonFrame = self.sendButton.frame
        var frame = self.textInput.frame
        //var toolbarFrame = self.toolbar.frame
        var imageFrame = self.image.frame
        // var textFrame = self.textfieldFrame.frame    var offset = (rect.height - ((self.view.frame.height - self.messageText.frame.origin.y) + self.messageText.frame.size.height)) + 80
        //   var offset = (rect.height - ((self.view.frame.height - self.messageText.frame.origin.y) + self.messageText.frame.size.height)) + 80
        var offset = (rect.height - ((self.view.frame.height - self.textInput.frame.origin.y) + self.textInput.frame.size.height))+80
        frame.origin.y = self.textInput.frame.origin.y - rect.height
        buttonFrame.origin.y = self.sendButton.frame.origin.y - rect.height
       // toolbarFrame.origin.y = self.toolbar.frame.origin.y - rect.height
        imageFrame.origin.y = self.image.frame.origin.y - rect.height
        //textFrame.origin.y = self.textfieldFrame.frame.origin.y - rect.height
        UIView.animateWithDuration(0.1, animations: {
            //            self.textfieldFrame.frame = textFrame
            
            //self.ChatView.frame.height -= self.messageText.frame.height + self.toolbar.frame.height
            var customheight = self.textInput.frame.height + rect.height
            var x = self.ChatView.frame.height - customheight
            
            var tableFrame = CGRectMake(self.ChatView.frame.origin.x, self.ChatView.frame.origin.y, self.ChatView.frame.width,x )
            self.ChatView.frame = tableFrame
            self.sendButton.frame = buttonFrame
            self.textInput.frame = frame
                        self.image.frame = imageFrame
            //var imageFrame = CGRectMake(self.image.frame.origin.x, self.image.frame.origin.y, self.image.frame.width, self.image.frame.height)
            
        })
        UIView.commitAnimations()
        self.ChatView.scrollBubbleViewToBottomAnimated(true)
        
    }
    func keyboardDidHide(notification : NSNotification){
        //print("\(self.messageText.frame.origin)")
        var c :NSDictionary = notification.userInfo! // as NSDictionary
        
        var point:NSValue = NSValue(nonretainedObject: c.objectForKey(UIKeyboardFrameEndUserInfoKey))
        //var keyboardSize = let dict:NSDictionary = sender.userInfo as NSDictionary
        let s:NSValue = c.valueForKey(UIKeyboardFrameEndUserInfoKey) as NSValue;
        let rect :CGRect = s.CGRectValue();
        
        var buttonFrame = self.sendButton.frame
        var frame = self.textInput.frame
    //    var toolbarFrame = self.toolbar.frame
        // var textFrame = self.textfieldFrame.frame
        var imageFrame = self.image.frame
      //  var offset = (rect.height - ((self.view.frame.height - self.messageText.frame.origin.y) + self.messageText.frame.size.height)) + 80
        frame.origin.y = self.textInput.frame.origin.y + rect.height
        //toolbarFrame.origin.y = self.toolbar.frame.origin.y + rect.height
        imageFrame.origin.y = self.image.frame.origin.y + rect.height
         buttonFrame.origin.y = self.sendButton.frame.origin.y + rect.height
        //textFrame.origin.y = self.textfieldFrame.frame.origin.y - rect.height
        UIView.animateWithDuration(0.1, animations: {
            //            self.textfieldFrame.frame = textFrame
            
            //self.ChatView.frame.height -= self.messageText.frame.height + self.toolbar.frame.height
            var customheight = self.textInput.frame.height + rect.height
            var x = self.ChatView.frame.height + customheight
            
            var tableFrame = CGRectMake(self.ChatView.frame.origin.x, self.ChatView.frame.origin.y, self.ChatView.frame.width,x )
            self.ChatView.frame = tableFrame
            self.textInput.frame = frame
            self.sendButton.frame = buttonFrame
            self.image.frame = imageFrame
            
        })
        UIView.commitAnimations()
        
        self.ChatView.scrollBubbleViewToBottomAnimated(true)
        
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "groupsView" {
            var dest = segue.destinationViewController as GroupsViewController
            dest.name = self.name
            self.inputStream.close()
            self.outputStream.close()
            
        }
    }
    func bubbleTableView(tableView: UIBubbleTableView!, dataForRow row: Int) -> NSBubbleData! {
        //self.ChatView.scrollBubbleViewToBottomAnimated(true)
        var obj = bubbleData.objectAtIndex(row) as NSBubbleData
        
        return bubbleData.objectAtIndex(row) as NSBubbleData
    }
    func rowsForBubbleTable(tableView: UIBubbleTableView!) -> Int {
        return bubbleData.count
        
    }
   
    
    func initNetworkCommunication() {
        
        
        //  declari niste stream-uri din care citesti si in care  scrii
        var readStream:Unmanaged<CFReadStream>?;
        var writeStream:Unmanaged<CFWriteStream>?     ;
        //localhost = 86.120.239.5
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,"localhost",8023,&readStream,&writeStream)
        
        inputStream = readStream!.takeUnretainedValue()
        outputStream = writeStream!.takeUnretainedValue()
        
        self.inputStream!.delegate=self
        self.outputStream!.delegate = self
        
        self.inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        
        //println("\(UsernameField.text)");
        var msg = "iam:\(self.name).groupView\r\n"
        
        //self.myname = msg.componentsSeparatedByString(":")[1] as NSString
        //println("\(msg)")
        var ptr = msg.nulTerminatedUTF8
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
        
    }
    func stream(theStream:NSStream , handleEvent streamEvent: NSStreamEvent) {
        switch(streamEvent){
        case NSStreamEvent.OpenCompleted :
            
            var ceva = ""
        case NSStreamEvent.HasBytesAvailable :
            if (theStream == inputStream){
                
                var len : Int
                var buffer : UnsafePointer<uint_fast8_t>!
                while (inputStream.hasBytesAvailable){
                    let buffer2=UnsafeMutablePointer<UInt8>.alloc(12230);
                    len = inputStream.read(buffer2, maxLength: 12230)
                    if(len>0){
                        var output :NSString = NSString(bytes: buffer2, length: len, encoding: NSASCIIStringEncoding)
                        
                        if ( output != ""){
                            
                        
                       
                            self.messageReceived(output)
                            //println("\(output)")
                            //ChatView.reloadData()
                        }
                    }
                }
                
            }
        case NSStreamEvent.ErrorOccurred :
            var x = 1
        case NSStreamEvent.EndEncountered :
            
            var connectionAlert = UIAlertView(title: "Connection Error", message: "The server is down you can not do anything in this mode", delegate: self, cancelButtonTitle: "Close")
            self.view.addSubview(connectionAlert)
            connectionAlert.show()
        default:
            var ceva = ""
            
        }
    }
    func messageReceived(output:NSString){
        if output.containsString("has joined"){
            //println("-------------->you have joined<-------------")
        }
        else if output.containsString("message from the group"){
            println("Intra aici")
            //you have a message from the group:ceva and person:dsa with the message
            //                                ce mai faciqweqweqweqweweweq
            var per = output.componentsSeparatedByString("and person:")[1] as NSString
            var person = per.componentsSeparatedByString(" ")[0] as NSString
            var msg = output.componentsSeparatedByString("with the message\n")[1] as NSString
            var dataFromMe = NSBubbleData(text: "\(person) said:\n\n\(msg)", date: NSDate(timeIntervalSinceNow: 0), type: BubbleTypeSomeoneElse)
           // dataFromMe.avatar = imageFromTable
            bubbleData[pos] = dataFromMe
            //bubbleData[pos] = "\(output)"
            //messages[pos] = ceva;
            pos++
            self.ChatView.reloadData()
        }
            
        else {
            println("\(output) sad sda asd as ")
        }

    }
    
    @IBAction func sendMessage(sender: AnyObject) {
        
        var msg = "msgtogroup:\(self.group):\(self.textInput.text):\(self.name):dasdsa"
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        var paths2 :NSArray = NSSearchPathForDirectoriesInDomains(.DocumentDirectory , .UserDomainMask, true)
        var docsDirectory :NSString = paths2.objectAtIndex(0) as NSString
        var pathing = docsDirectory.stringByAppendingPathComponent("test.png")
        var imageFromTable = UIImage(contentsOfFile: pathing)
        
        var ceva = "ALTA BUCURIE"
        var partOfIt = msg
        ceva = "\(partOfIt)"
        
        var dataFromMe = NSBubbleData(text: "\(self.textInput.text)", date: NSDate(timeIntervalSinceNow: 0), type: BubbleTypeMine)
        dataFromMe.avatar = imageFromTable
        bubbleData[pos] = dataFromMe
        //messages[pos] = ceva;
        pos++
        self.ChatView.reloadData()
        self.textInput.text = ""
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    ove
    rride func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
