//
//  myContactDetailsViewController.swift
//  ChatClien
//
//  Created by Tudor on 22/08/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit

class myContactDetailsViewController: UIViewController , UITableViewDelegate {

    @IBOutlet var ContactName: UILabel!
    @IBOutlet var ContactImage: UIImageView!
    @IBOutlet var ContactDetails: UITableView!
    
    var contactInfo : NSDictionary! = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ContactDetails.delegate = self
        
        self.populateContactData()
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func populateContactData(){
        var fname = contactInfo.objectForKey("firstName") as NSString
        var lname = contactInfo.objectForKey("lastName") as NSString
        var fullName = "\(fname) \(lname) "
        self.ContactName.text = fullName
        
        if self.contactInfo.objectForKey("image") != nil {
            ContactImage.image = self.contactInfo.objectForKey("image") as UIImage
        }
        
        ContactDetails?.reloadData()
        
        
    }

   func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 3
    }
  func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if section == 0 {
            return 2
        }
        else if section == 1 {
            return 2
        }
        else {
            return 3
    
        }
    }
    func tableView(tableView: UITableView!,
        titleForHeaderInSection section: Int) -> String!{
            switch (section) {
            case 0 :
                return "Phone number"
            case 1 :
                return "E-mail address"
            case 2 :
                return "Address Info"
            default :
                return ""
            }
    }
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell!{
        var cell :UITableViewCell = self.ContactDetails.dequeueReusableCellWithIdentifier("DetailCell") as UITableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "DetailCell")
        }
        var cellText = ""
        var detailText = ""
        var x = ""
        switch (indexPath.section) {
            case 0 :
                switch indexPath.row{
                    case 0 :
                        cellText = contactInfo.objectForKey("mobileNumber") as NSString
                        detailText = "Mobile Number"
                    default:
                        x = ""
                    
            }
            case 1 :
                switch indexPath.row{
                    case 0 :
                        cellText = contactInfo.objectForKey("homeEmail") as NSString
                        detailText = "Home Email"
                    case 1 :
                        cellText = contactInfo.objectForKey("workEmail") as NSString
                        detailText = "Work Email"
                    default :
                        x = ""
                    
                    
            }
            case 2 :
                switch indexPath.row {
                    case 0:
                        cellText = contactInfo.objectForKey("address") as NSString
                        detailText = "Street Adress"
                    case 1:
                        cellText = contactInfo.objectForKey("zip") as NSString
                        detailText = "Zip Code"
                    case 2:
                        cellText = contactInfo.objectForKey("city") as NSString
                        detailText = "City"
                    default :
                        x = ""
            }
            default:
                x = ""
        }
        
        
        cell.textLabel.text = cellText
        cell.detailTextLabel.text = detailText
        return cell
    }
    
    
    func setDictContactDetails(newDict :NSDictionary) {
        self.contactInfo = newDict
    }
}
