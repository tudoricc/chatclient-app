//
//  ViewController.swift
//  ChatClien
//
//  Created by iOS Developer on 29/07/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit


class ViewController: UIViewController,NSStreamDelegate,RNFrostedSidebarDelegate , UITextFieldDelegate{
                            
    @IBOutlet var JoinButton: UIButton!
  
    @IBOutlet var UsernameField: UITextField!

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var passwordField: UITextField!
    internal var inputStream : NSInputStream!
    internal var outputStrean : NSOutputStream!
    var name: String!
    var optionIndices = NSMutableIndexSet( index: 1)
 
    
    @IBOutlet var loginLabel: UILabel!
   
    
    let backGroundArray = [UIImage(named: "img1.jpg"),UIImage(named:"img2.jpg"), UIImage(named: "img3.jpg"), UIImage(named: "img4.jpg")]
    var idx: Int = 0
    
@IBOutlet var signUpButton: UIButton!
    
    override func viewDidLoad() {
        
        //for those that are interested: THE WHOLE app connects to a server.for the development part I did something with xampp so you will see a lot of localhost,in the future it would be advised to change they word localhost with the ip of the computer that keeps the server running.
        
        //This is good for all the view not just this one
        
        super.viewDidLoad()
        UIView.animateWithDuration(0.3, animations: {
        self.loginLabel.frame = CGRectMake(self.loginLabel.frame.origin.x - 100, self.loginLabel.frame.origin.y, self.loginLabel.frame.width, self.loginLabel.frame.height)
        })
self.signUpButton.layer.cornerRadius = 5

        
        UIView.commitAnimations()
        self.UsernameField.delegate = self
        self.passwordField.delegate = self
        self.JoinButton.alpha = 1
        self.UsernameField.alpha = 1
        self.passwordField.alpha = 1
        //UsernameField.addTarget(self, action: "textFieldDidChange", forControlEvents: UIControlEvents.EditingChanged)
        //passwordField.addTarget(self, action: "textFieldDidChange", forControlEvents: UIControlEvents.EditingChanged)
        
        var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Dark)) as UIVisualEffectView
        visualEffectView.frame  = self.view.frame
        visualEffectView.alpha = 0.3
        self.imageView.image = UIImage(named : "img1")
        self.imageView.addSubview(visualEffectView)
        
        
        
        NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: "changeImage", userInfo: nil, repeats: true)
       // self.loginButton(false)

        // Do any additional setup after loading the view, typically from a nib.
        self.initNetworkCommunication()
        name=UsernameField.text
        
        //        f.autocorrectionType = UITextAutocorrectionTypeNo

 //       self.UsernameField.autocapitalizationType = false
        
    }
    func changeImage(){
        if idx == backGroundArray.count-1{
            idx = 0
        }
        else{
            idx++
        }
        var toImage = backGroundArray[idx];
        UIView.transitionWithView(self.imageView, duration: 3, options: .TransitionCrossDissolve, animations: {self.imageView.image = toImage}, completion: nil)
        
    }

    
    func initNetworkCommunication() {
        
        //declari niste stream-uri din care citesti si in care  scrii
        var readStream:Unmanaged<CFReadStream>?;
        var writeStream:Unmanaged<CFWriteStream>?     ;
        
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,"localhost",8023,&readStream,&writeStream)
        
        inputStream = readStream!.takeUnretainedValue()
        outputStrean = writeStream!.takeUnretainedValue()
        
        self.inputStream!.delegate=self
        self.outputStrean!.delegate = self
        
        self.inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStrean!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStrean.open()
                
     //   var msg = "iam:\(self.name).firstView\r\n"
        
       // self.myname = msg.componentsSeparatedByString(":")[1] as NSString
        //println("\(msg)")
       // var ptr = msg.nulTerminatedUTF8
        //var res = outputStrean.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
    }
    
    
    
    func stream(theStream:NSStream , handleEvent streamEvent: NSStreamEvent) {
        switch(streamEvent){
        case NSStreamEvent.OpenCompleted :
            //println("Opened correctly")
            var ceva = ""
        case NSStreamEvent.HasBytesAvailable :
            if (theStream == inputStream){
                
                var len : Int
                var buffer : UnsafePointer<uint_fast8_t>!
                while (inputStream.hasBytesAvailable){
                    let buffer2=UnsafeMutablePointer<UInt8>.alloc(12230);
                    len = inputStream.read(buffer2, maxLength: 12230)
                    if(len>0){
                        var output :NSString = NSString(bytes: buffer2, length: len, encoding: NSASCIIStringEncoding)
                        //aici trebuie sa faci si tu un split sa vezi daca e numele tau cel de la care a trimis
                        //si daca e sa afisezi cu alta culoare si vezi daca nu e numele tau afisezi c u alta culoare
                        
                        //REMINDER: nu uita de treaba aia cu variabila dintr-un controller in altul
                        if ( output == ""){
                            
                        }
                        else{
                            if output.containsString("you are succhessfully logged"){
                                println("ceva")
                                var FirstView :ViewController2
                               
                                FirstView = self.storyboard?.instantiateViewControllerWithIdentifier("SecondView") as ViewController2
                                FirstView.name = UsernameField.text
                                FirstView.inputStream = self.inputStream
                                FirstView.outputStrean = self.outputStrean
                                self.inputStream.close()
                                self.outputStrean.close()
                                self.presentViewController(FirstView, animated: true, completion:nil)
                                self.viewWillDisappear(true)
                                self.loginLabel.text = ""
                                break
                            }
                            else{
                                println("not so fast")
                                self.loginLabel.text = "Wrong credentials"
//                                self.inputStream.close()
//                                self.outputStrean.close()
                            }
                            // println("Server said : \(output)")
                            //self.messageReceived(output)
                            
                            //println(" \(message)  ")
                           // tableView?.reloadData()
                            
                        }
                    }
                }
                
            }
        case NSStreamEvent.ErrorOccurred :
            // println("Can not connect to the host")
            //  var connectionAlert = UIAlertView(title: "Connection Error", message: "The server is down you can not do anything in this mode", delegate: self, cancelButtonTitle: "Close")
            //self.view.addSubview(connectionAlert)
            // connectionAlert.show()
            var x = 1
        case NSStreamEvent.EndEncountered :
            // println("Here it is")
            var connectionAlert = UIAlertView(title: "Connection Error", message: "The server is down you can not do anything in this mode", delegate: self, cancelButtonTitle: "Close")
            self.view.addSubview(connectionAlert)
            connectionAlert.show()
        default:
            var ceva = ""
            //println("Unknown event")
        }
    }

    
    

    

    func getName()->String!{
        return self.name
    }
    func getInputStream()->NSInputStream{
        return self.inputStream
    }
    func getOutputStream()->NSOutputStream{
        return self.outputStrean
    }
    @IBAction func joinChat (sender:UIButton){
       var name = self.UsernameField.text
        var password = self.passwordField.text
        var msg = "login:\(name):\(password):dsadsaa\r\n"
        //variabila name din viewController2 e initializata cu asta.
        var FirstView :ViewController2
        FirstView = self.storyboard?.instantiateViewControllerWithIdentifier("SecondView") as ViewController2
        FirstView.name = UsernameField.text
        FirstView.inputStream = self.inputStream
        FirstView.outputStrean = self.outputStrean
        
        var ThirView : AddFriendViewController
        //ThirView = self.storyboard.instantiateViewControllerWithIdentifier("addFriend") as AddFriendViewController
        
        
        
        var ChatView :ViewController3
        ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView") as ViewController3
        ChatView.inputStream = self.inputStream
        ChatView.outputStream = self.outputStrean
        
        //self.presentViewController(FirstView, animated: true, completion:nil)
        var response : NSString!
        var actualResponse : NSData!
        //response = "iam:\(UsernameField.text)"
        
        //daca iti pica functia asta inseamna ca e de   aici
       // actualResponse = response.dataUsingEncoding(NSASCIIStringEncoding)
        
        //iau continutul raspunsului final : iam:tudor <-asta inseamna raspuns final
        var length : UInt8

        var res:Int
        //println("\(UsernameField.text)");
        //var msg = "iam:\(UsernameField.text)\r\n"
        //println("\(msg)")
        var ptr = msg.nulTerminatedUTF8
        res = outputStrean.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        self.viewWillDisappear(true)
      //  ViewController2.viewWillAppear(UIViewController)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    func textFieldDidBeginEditing(textField: UITextField!) {
        textField.text = ""
}
    
    

}
extension UIColor{
    
    class func colorWithHex(hex: String, alpha: CGFloat = 1.0) -> UIColor {
    var rgb: CUnsignedInt = 0;
    let scanner = NSScanner(string: hex)

    if hex.hasPrefix("#") {
        // skip '#' character
        scanner.scanLocation = 1
    }
    scanner.scanHexInt(&rgb)

    let r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
    let g = CGFloat((rgb & 0xFF00) >> 8) / 255.0
    let b = CGFloat(rgb & 0xFF) / 255.0

    return UIColor(red: r, green: g, blue: b, alpha: alpha)
}
}


