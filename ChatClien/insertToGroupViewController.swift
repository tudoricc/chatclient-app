//
//  insertToGroupViewController.swift
//  ChatClien
//
//  Created by Tudor on 17/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit

class insertToGroupViewController: UIViewController,UITableViewDelegate,UITableViewDataSource ,UIAlertViewDelegate,UISearchBarDelegate, UISearchDisplayDelegate ,NSStreamDelegate {
    var name :NSString!
    //var messages = []
    var inputStream :NSInputStream!
    var outputStream:NSOutputStream!
    var messages = [Candy]()
    var firstTime = true
    @IBOutlet var table : UITableView!
    var filteredMessages = [Candy]()
    var msg = "s"
    
    override func viewDidLoad() {
        
        self.initNetworkCommunication()
        
        table.delegate = self
        table.dataSource = self
        if firstTime == true {
        NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "sendMessage2", userInfo: nil, repeats: false)
        
        }
        else{
            
        }
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    
    func tableView(tableView: UITableView!, accessoryTypeForRowWithIndexPath indexPath: NSIndexPath!) -> UITableViewCellAccessoryType {
        return UITableViewCellAccessoryType.Checkmark
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //ask for a reusable cell from the tableview, the tableview will create a new one if it doesn't have any
        let cell = self.table.dequeueReusableCellWithIdentifier("resultsCell") as UITableViewCell
        
        var candy :Candy
        // Check to see whether the normal table or search results table is being displayed and set the Candy object from the appropriate array
        if tableView == self.searchDisplayController!.searchResultsTableView {
            candy = filteredMessages[indexPath.row]
        } else {
            candy = messages[indexPath.row]
        }
        
        // Configure the cell
        cell.textLabel!.text = candy.name
//        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            var y = filteredMessages[indexPath.row].name as NSString
            if y == ""  {
                println("you clicked it")
            }
            else{
                println("you have something")
                var msg2 = "makegroup:\(y):\(self.name):dsa"
                self.msg = msg2
            //    var res = outputStream.write(msg, maxLength:msg2.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
            }
        }
        else {
            var x = messages[indexPath.row].name as NSString
            if x == ""  {
                println("you clicked it")
            }
            else{
                println("you have something")
                
              var msg2 = "makegroup:\(x):\(self.name):dsa"
              self.msg = msg2
              //  var res = outputStream.write(msg, maxLength:msg2.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
                
            }
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if tableView == self.searchDisplayController!.searchResultsTableView {
                return self.filteredMessages.count
            }
            else {
                return  self.messages.count
            }
        }
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        println("saddsa")
    }
    func filterContentForSearchText(searchText: String) {
        
        self.filteredMessages = self.messages.filter({( candy: Candy) -> Bool in
            
            let stringMatch = candy.name.rangeOfString(searchText)
            return (stringMatch != nil)
        })
    }
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        self.filterContentForSearchText(searchString)
        return true
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text)
        return true
    }
    
    
    
    
    
    func initNetworkCommunication() {
        
        
        //  declari niste stream-uri din care citesti si in care  scrii
        var readStream:Unmanaged<CFReadStream>?;
        var writeStream:Unmanaged<CFWriteStream>?     ;
        //localhost = 86.120.239.5
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,"localhost",8023,&readStream,&writeStream)
        
        inputStream = readStream!.takeUnretainedValue()
        outputStream = writeStream!.takeUnretainedValue()
        
        self.inputStream!.delegate=self
        self.outputStream!.delegate = self
        
        self.inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        
        //println("\(UsernameField.text)");
        var msg = "iam:\(self.name).groupView\r\n"
        
        //self.myname = msg.componentsSeparatedByString(":")[1] as NSString
        //println("\(msg)")
        var ptr = msg.nulTerminatedUTF8
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
        
    }
    
    
    func stream(theStream:NSStream , handleEvent streamEvent: NSStreamEvent) {
        switch(streamEvent){
        case NSStreamEvent.OpenCompleted :
            
            var ceva = ""
        case NSStreamEvent.HasBytesAvailable :
            if (theStream == inputStream){
                
                var len : Int
                var buffer : UnsafePointer<uint_fast8_t>!
                while (inputStream.hasBytesAvailable){
                    let buffer2=UnsafeMutablePointer<UInt8>.alloc(12230);
                    len = inputStream.read(buffer2, maxLength: 12230)
                    if(len>0){
                        var output :NSString = NSString(bytes: buffer2, length: len, encoding: NSASCIIStringEncoding)
                        
                        if ( output == ""){
                            
                        }
                        else{
                            if output.containsString("has joined"){
                                println("-------------->you have joined<-------------")
                            }
                            else if output.containsString("available groups"){
                                println("Intra aici")
                                var smth = output.componentsSeparatedByString("\n")[1] as NSString
                                println("groups are : \(smth)")
                                var smthelse = smth.componentsSeparatedByString("groups:")[1] as NSString
                                var listOfGroups = smthelse.componentsSeparatedByString(":")
                                println("only the groups are : \(listOfGroups)")
                                for group in listOfGroups {
                                    var cuvant = "\(group)"
                                    var candy = Candy(category: "All", name: cuvant)
                                    self.messages.append(candy)
                                }
                                self.table.reloadData()
                            }
                            else if output.containsString("You are now part of the group"){
                                println("we have an error")
                            }
                            else if output.containsString("message from the group"){
                                var group = output.componentsSeparatedByString("from the group:")[1] as NSString
                                var groupName = group.componentsSeparatedByString("and person")[0] as NSString
                                //var grupName = groupName.componentsSeparatedByString("from group:")[1] as NSString
                                var per = output.componentsSeparatedByString("and person:")[1] as NSString
                                var person = per.componentsSeparatedByString(" ")[0] as NSString
                                var msg2 = output.componentsSeparatedByString(" with the message\n")[1] as NSString
                                var ceva = msg2.componentsSeparatedByString("you have a message from the group")[0] as NSString
                                var banner :ALAlertBanner
                                banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "message from group: \(groupName)", subtitle: "\(person) said \(ceva)", tappedBlock: { (banner :ALAlertBanner!) in
                                    var ChatView : groupChatController
                                    //nu lasa asa pune sa afiseze ceview vri tu
                                    
                                    ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("groupChat") as groupChatController
                                    ChatView.group = groupName
                                    ChatView.name = self.name
                                    
                                    self.inputStream.close()
                                    self.outputStream.close()
                                    self.viewWillDisappear(true)
                                    self.presentViewController(ChatView, animated: true, completion:nil)
                                })
                                banner.exclusiveTouch = true
                                banner.show()
                            }

                            else if output.containsString("you have a message") {
                                println("YOU HAVE A MESSAGE")
                                var fromuser =  output.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[2] as NSString
                                var onlythemessage = output.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[6] as NSString
                                var banner :ALAlertBanner
                                banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "message from \(fromuser)", subtitle: onlythemessage, tappedBlock: { (banner :ALAlertBanner!) in
                                    var ChatView    : ViewController3
                                    //nu lasa asa pune sa afiseze ceview vri tu
                                    
                                    ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView") as ViewController3
                                    ChatView.nameofReceiver = fromuser
                                    ChatView.nameofSender = self.name
                                    
                                    self.inputStream.close()
                                    self.outputStream.close()
                                    self.viewWillDisappear(true)
                                    self.presentViewController(ChatView, animated: true, completion:nil)
                                })
                                banner.exclusiveTouch = true
                                banner.show()
                                var localnotification:UILocalNotification = UILocalNotification()
                                localnotification.category = "FIRST_CATEGORY"
                                localnotification.alertBody = "\(fromuser) said: \(onlythemessage)"
                                localnotification.fireDate = NSDate(timeIntervalSinceNow: 5)
                                UIApplication.sharedApplication().scheduleLocalNotification(localnotification)

                            }
    
                            else {
                                println("\(output) sad sda asd as ")
                            }
                            
                        }
                    }
                }
                
            }
        case NSStreamEvent.ErrorOccurred :
            var x = 1
        case NSStreamEvent.EndEncountered :
            
            var connectionAlert = UIAlertView(title: "Connection Error", message: "The server is down you can not do anything in this mode", delegate: self, cancelButtonTitle: "Close")
            self.view.addSubview(connectionAlert)
            connectionAlert.show()
        default:
            var ceva = ""
            
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "groupsList" {
            self.inputStream.close()
            self.outputStream.close()
            var dest = segue.destinationViewController as GroupsViewController
            dest.name = self.name
        }
    }
    
    
    func sendMessage2(){
        var msg = "getgroups:\(self.name):sdasa"
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        println("I sent the request")
    }
    
    @IBAction func submit(sender: AnyObject) {
        
        var res = outputStream.write(self.msg, maxLength:self.msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
