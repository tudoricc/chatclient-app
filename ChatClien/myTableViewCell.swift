//
//  myTableViewCell.swift
//  ChatClien
//
//  Created by Tudor on 03/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit

class myTableViewCell: UITableViewCell {


    @IBOutlet weak var customLabel: UILabel!
    @IBOutlet weak var customImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
