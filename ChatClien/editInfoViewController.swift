//
//  editViewController.swift
//  ChatClien
//
//  Created by Tudor on 09/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit
import MediaPlayer
class editViewController: UIViewController , NSStreamDelegate , UIImagePickerControllerDelegate , UINavigationControllerDelegate {

    @IBOutlet var label: UILabel!
    @IBOutlet var options: UISegmentedControl!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var passwordLabel: UILabel!
    @IBOutlet var nameValue: UITextField!
    @IBOutlet var emailValue: UITextField!
    @IBOutlet var phoneValue: UITextField!
    @IBOutlet var passwordValue: UITextField!
    @IBOutlet var personImage: UIImageView!
    
    var name :NSString!
    var inputStream : NSInputStream!
    var outputStream : NSOutputStream!
    var imagePicker :UIImagePickerController!
    var image :UIImageView!
    override func viewDidLoad() {
        
       // println("-----------------aici incepe view-ul acesta-------------------------------------")
        self.navBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
//        self.navBar.setBackgroundImage(UIImage(), barMetrics: UIBarMetrics.Default)
        self.navBar.shadowImage = UIImage()
        self.navBar.translucent = true
        self.navBar.backgroundColor = UIColor.clearColor()
        var paths2 :NSArray = NSSearchPathForDirectoriesInDomains(.DocumentDirectory , .UserDomainMask, true)
        var docsDirectory :NSString = paths2.objectAtIndex(0) as NSString
        var pathing = docsDirectory.stringByAppendingPathComponent("test.png")
        var imageFromTable = UIImage(contentsOfFile: pathing)
        
        self.personImage.image = imageFromTable
        self.personImage.layer.cornerRadius = self.personImage.frame.size.width / 4
        self.personImage.hidden = false
        
        self.personImage.clipsToBounds = true
        self.nameValue.text = self.name
        
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "profile-bg").drawInRect(self.view.bounds)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
        self.initNetworkCommunication()
        
        var msg = "getimage:\(self.name):das"
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
       
        //personImage.hidden = true
        //personImage.image = UIImage(named: "user")
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func stream(theStream:NSStream , handleEvent streamEvent: NSStreamEvent) {
        switch(streamEvent){
        case NSStreamEvent.OpenCompleted :
            //println("Opened correctly")
            var ceva = ""
        case NSStreamEvent.HasBytesAvailable :
            if (theStream == inputStream){
                
                var len : Int
                var buffer : UnsafePointer<uint_fast8_t>!
                while (inputStream.hasBytesAvailable){
                    let buffer2=UnsafeMutablePointer<UInt8>.alloc(12230);
                    len = inputStream.read(buffer2, maxLength: 12230)
                    if(len>0){
                        var output :NSString = NSString(bytes: buffer2, length: len, encoding: NSASCIIStringEncoding)
                        //aici trebuie sa faci si tu un split sa vezi daca e numele tau cel de la care a trimis
                        //si daca e sa afisezi cu alta culoare si vezi daca nu e numele tau afisezi c u alta culoare
                        
                        //REMINDER: nu uita de treaba aia cu variabila dintr-un controller in altul
                        if ( output == ""){
                            
                        }
                        else{
                            if output.containsString("has joined"){
                                println("you have successfully logged in")
                            }
                            else if output.containsString("------------------------->no such file<-------------------------------"){
                                println("Nu ai imagine")
                            }
                            else if output.containsString("image"){
                                println("------------------------>dsadsadasdsasad ai primit imagine<-----------------------------")
                                var binaryData: NSData
                                let string : NSString  = output.componentsSeparatedByString(":")[0] as NSString
                                binaryData = string.dataUsingEncoding(NSUTF8StringEncoding)!
                          
                                var data = UIImage(data: binaryData)
                                self.personImage.image = data
                                self.personImage.hidden = true
                            }
                            else if output.containsString("message from the group"){
                                var group = output.componentsSeparatedByString("from the group:")[1] as NSString
                                var groupName = group.componentsSeparatedByString("and person")[0] as NSString
                                //var grupName = groupName.componentsSeparatedByString("from group:")[1] as NSString
                                var per = output.componentsSeparatedByString("and person:")[1] as NSString
                                var person = per.componentsSeparatedByString(" ")[0] as NSString
                                var msg2 = output.componentsSeparatedByString(" with the message\n")[1] as NSString
                                var ceva = msg2.componentsSeparatedByString("you have a message from the group")[0] as NSString
                                var banner :ALAlertBanner
                                banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "message from group: \(groupName)", subtitle: "\(person) said \(ceva)", tappedBlock: { (banner :ALAlertBanner!) in
                                    var ChatView : groupChatController
                                    //nu lasa asa pune sa afiseze ceview vri tu
                                    
                                    ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("groupChat") as groupChatController
                                    ChatView.group = groupName
                                    ChatView.name = self.name
                                    
                                    self.inputStream.close()
                                    self.outputStream.close()
                                    self.viewWillDisappear(true)
                                    self.presentViewController(ChatView, animated: true, completion:nil)
                                })
                                banner.exclusiveTouch = true
                                banner.show()
                            }

                            else if output.containsString("you have a message") {
                                println("YOU HAVE A MESSAGE")
                                var fromuser =  output.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[2] as NSString
                                var onlythemessage = output.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[6] as NSString
                                var banner :ALAlertBanner
                                banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "message from \(fromuser)", subtitle: onlythemessage, tappedBlock: { (banner :ALAlertBanner!) in
                                    var ChatView    : ViewController3
                                    //nu lasa asa pune sa afiseze ceview vri tu
                                    
                                    ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView") as ViewController3
                                    ChatView.nameofReceiver = fromuser
                                    ChatView.nameofSender = self.name
                                    
                                    self.inputStream.close()
                                    self.outputStream.close()
                                    self.viewWillDisappear(true)
                                    self.presentViewController(ChatView, animated: true, completion:nil)
                                })
                                banner.exclusiveTouch = true
                                banner.show()
                                var localnotification:UILocalNotification = UILocalNotification()
                                localnotification.category = "FIRST_CATEGORY"
                                localnotification.alertBody = "\(fromuser) said: \(onlythemessage)"
                                localnotification.fireDate = NSDate(timeIntervalSinceNow: 5)
                                UIApplication.sharedApplication().scheduleLocalNotification(localnotification)

                            }

                            else {
                                println("-->]\(output)<------------")
                                var binaryData: NSData
                                let string : NSString  = output.componentsSeparatedByString(":")[0] as NSString
                                binaryData = string.dataUsingEncoding(NSUTF8StringEncoding)!
                                
                                var data = UIImage(data: binaryData)
                                self.personImage.image = data
                                self.personImage.hidden = true
                            }
                        }
                    }
                }
                
            }
        case NSStreamEvent.ErrorOccurred :
            // println("Can not connect to the host")
            //  var connectionAlert = UIAlertView(title: "Connection Error", message: "The server is down you can not do anything in this mode", delegate: self, cancelButtonTitle: "Close")
            //self.view.addSubview(connectionAlert)
            // connectionAlert.show()
            var x = 1
        case NSStreamEvent.EndEncountered :
            // println("Here it is")
            var connectionAlert = UIAlertView(title: "Connection Error", message: "The server is down you can not do anything in this mode", delegate: self, cancelButtonTitle: "Close")
            self.view.addSubview(connectionAlert)
            connectionAlert.show()
        default:
            var ceva = ""
            //println("Unknown event")
        }
    }
    func initNetworkCommunication() {
        
        
        //  declari niste stream-uri din care citesti si in care  scrii
        var readStream:Unmanaged<CFReadStream>?;
        var writeStream:Unmanaged<CFWriteStream>?     ;
        //localhost = 86.120.239.5
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,"localhost",8023,&readStream,&writeStream)
        
        inputStream = readStream!.takeUnretainedValue()
        outputStream = writeStream!.takeUnretainedValue()
        
        self.inputStream!.delegate=self
        self.outputStream!.delegate = self
        
        self.inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        
        //println("\(UsernameField.text)");
        var msg = "iam:\(self.name).editView\r\n"
        
        var myname = msg.componentsSeparatedByString(":")[1] as NSString
        //println("\(msg)")
        var ptr = msg.nulTerminatedUTF8
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
        
    }


    
    @IBAction func actionForButtons(sender: UISegmentedControl) {
        var itemPushed = sender.selectedSegmentIndex
        if itemPushed == 0 {
            label.hidden = false
            nameLabel.hidden = false
            nameValue.hidden = false
            
            emailLabel.hidden = false
            emailValue.hidden = false
            
            
            phoneLabel.hidden = false
            phoneValue.hidden = false
            
            passwordLabel.hidden = false
            passwordValue.hidden = false

            personImage.hidden = false
        }
        else{
           label.hidden = true
            nameLabel.hidden = true
            nameValue.hidden = true

            emailLabel.hidden = true
            emailValue.hidden = true

            
            phoneLabel.hidden = true
            phoneValue.hidden = true

            passwordLabel.hidden = true
            passwordValue.hidden = true
            
            
            
            personImage.hidden = false
        }
    
        
    }
    @IBAction func sendModification(sender: UIButton) {
       
            println("Second honey")
            var mediaPicker = UIImagePickerController()
            imagePicker = UIImagePickerController()
            ///set the delegate
            imagePicker.delegate  =  self
            
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            }
            else {
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            }
            self.presentViewController(imagePicker, animated: true, completion: nil)
        
    }
//    func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]!) {
//        let image = info[UIImagePickerControllerOriginalImage] as UIImage
//        self.personImage.image = image
//        self.personImage.hidden = false
//        self.dismissViewControllerAnimated(true, completion: nil)
//
//    }
    @IBOutlet var navBar: UINavigationBar!
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        //self.personImage.image = image
        println("Ddsadas")
        self.personImage.hidden = false
       // self.personImage.layer.cornerRadius = self.personImage.frame.size.width/2
        self.personImage.clipsToBounds = true

        var iamgeData :NSData = UIImagePNGRepresentation(image)
        var paths : NSArray  = NSSearchPathForDirectoriesInDomains(.DocumentDirectory , .UserDomainMask, true)
        var docDirectory :NSString = paths.objectAtIndex(0) as NSString
        var imagePath :NSString = docDirectory.stringByAppendingPathComponent("test.png")
        var imgData = UIImagePNGRepresentation(image)
       imgData.writeToFile(imagePath, atomically: true)

        
        var paths2 :NSArray = NSSearchPathForDirectoriesInDomains(.DocumentDirectory , .UserDomainMask, true)
        var docsDirectory :NSString = paths2.objectAtIndex(0) as NSString
        var pathing = docsDirectory.stringByAppendingPathComponent("test.png")
        var imageFromTable = UIImage(contentsOfFile: pathing)
    
        self.personImage.image = imageFromTable
        self.personImage.layer.cornerRadius = self.personImage.frame.size.width/2
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController!) {
        self.dismissViewControllerAnimated(true, completion: nil)

    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "backFromEdit" {
            self.inputStream.close()
            self.outputStream.close()
            var viewRes = segue.destinationViewController as ViewController2
            viewRes.name = self.name
        }
    }
   
    @IBAction func submitRequest(sender: AnyObject) {
        
        if self.passwordValue != nil {
            var msg = "modifypass:\(self.name):\(self.passwordValue.text):dasds"
            var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        }
        if self.emailValue != nil {
            var msg = "modifyemail:\(self.name):\(self.emailValue.text):dsa"
            var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        }
       if self.phoneValue != nil {
            var msg = "modifyphone:\(self.name):\(self.phoneValue.text):dsasa"
            var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))

        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
