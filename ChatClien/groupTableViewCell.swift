//
//  groupTableViewCell.swift
//  ChatClien
//
//  Created by Tudor on 12/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit

class groupTableViewCell: UITableViewCell {
   
    @IBOutlet var groupName: UILabel!
    var itemText : NSString!
    var infoUsers : NSString!
    @IBOutlet var members: UILabel!
    var firstTime = 2
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var goToChat: UIButton!
   // @IBOutlet var members: UILabel!
    
    @IBOutlet var iamge: UIImageView!
    @IBAction func showInfo(sender: AnyObject) {
        println("show group's online users")
        if (firstTime % 2 == 0) {
            self.infoLabel.hidden = false
            firstTime++
        }
        else {
            self.infoLabel.hidden = true
            firstTime++
        }
    }
    @IBAction func goToChat(sender: AnyObject) {
        println("go to chat")
    }
    @IBAction func stopShowingInfo(sender:AnyObject) {
        self.infoLabel.hidden = true
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   }
