//
//  ViewController3.swift
//  ChatClien
//
//  Created by Tudor on 08/08/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit


class ViewController3: UIViewController , UITextViewDelegate ,UIBubbleTableViewDataSource, NSStreamDelegate{
    
    
    @IBOutlet var receiver: UILabel!
    @IBOutlet var senderLabel: UILabel!
    @IBOutlet var messageText: UITextView!
    //@IBOutlet var sendButton: UIButton!
    @IBOutlet var receiverLabel: UILabel!
    //@IBOutlet var messageText: UITextField!
    @IBOutlet var sendButton: UIBarButtonItem!
    
    @IBOutlet var textfieldFrame: UIView!
    @IBOutlet var toolbar: UIToolbar!
    @IBOutlet var backButton: UIBarButtonItem!
    @IBOutlet var ChatView: UIBubbleTableView!
    
    var allertManager : ALAlertBannerManager = ALAlertBannerManager()
    var nameofSender : NSString!
    var nameofReceiver : NSString!
    var inputStream : NSInputStream!
    var outputStream : NSOutputStream!
    var pos = 0
    var messages : NSMutableArray = [" "]
    var messagefrom :[(String,String)]!
    var futurePerson : NSString!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    var bubbleData :NSMutableArray! = NSMutableArray()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ChatView.bubbleDataSource = self
        ChatView.snapInterval = 120
        ChatView.showAvatars = true
        ChatView.reloadData()
        
        self.messageText.delegate = self
        self.view.addSubview(messageText)
        receiverLabel.text  = nameofReceiver
        
        self.messageText.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidShow:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidHide:", name:UIKeyboardDidHideNotification, object: nil)
        //self.messageText.sizeToFit()
        // self.outputStream.write(response, maxLength :response.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "sendMessage2", userInfo: nil, repeats: true)
        NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "sendMessage3", userInfo: nil, repeats: false)
        
        
        
        
        // self.messageText.layer.borderWidth = 0.4
        // self.messageText.layer.borderColor = UIColor.lightGrayColor().CGColor
        //self.messageText.layer.cornerRadius = 8.0
         // self.messageText.selectedRange = NSMakeRange(0, 0)
        self.messageText.autocorrectionType = UITextAutocorrectionType.No
        self.messageText.frame = CGRectMake(self.messageText.frame.origin.x,self.messageText.frame.origin.y,250,self.messageText.frame.height)
        self.initNetworkCommunication()
        
        self.toolbar.multipleTouchEnabled = true
        //        var lastIndex = max(0, self.ChatView.numberOfSections())
        //        var lastrowIndex = max(0, self.ChatView.numberOfRowsInSection(lastIndex))
        //
        //        var index = NSIndexPath(forItem: lastrowIndex, inSection: lastIndex)
        //        self.ChatView.scrollToRowAtIndexPath(index, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
        
        
        self.ChatView.scrollBubbleViewToBottomAnimated(true)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "", name: "actionOnePressed", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "showMessage:", name: "actionTwoPressed", object: nil)
        
    }
    
    
    func keyboardDidShow(notification :NSNotification){
        //println("A aparut")
        print("\(self.messageText.frame.origin)")
        //
        var c :NSDictionary = notification.userInfo! as NSDictionary
        var x = notification.userInfo
        
        var point:NSValue = NSValue(nonretainedObject: c.objectForKey(UIKeyboardFrameEndUserInfoKey))
        //var keyboardSize = let dict:NSDictionary = sender.userInfo as NSDictionary
        let s:NSValue = c.valueForKey(UIKeyboardFrameEndUserInfoKey) as NSValue;
        let rect :CGRect = s.CGRectValue();
        
        
        var frame = self.messageText.frame
        var toolbarFrame = self.toolbar.frame
        var imageFrame = self.image.frame
        // var textFrame = self.textfieldFrame.frame
        
        var offset = (rect.height - ((self.view.frame.height - self.messageText.frame.origin.y) + self.messageText.frame.size.height)) + 80
        frame.origin.y = self.messageText.frame.origin.y - rect.height
        toolbarFrame.origin.y = self.toolbar.frame.origin.y - rect.height
        imageFrame.origin.y = self.image.frame.origin.y - rect.height
        //textFrame.origin.y = self.textfieldFrame.frame.origin.y - rect.height
        UIView.animateWithDuration(0.1, animations: {
            //            self.textfieldFrame.frame = textFrame
            
            //self.ChatView.frame.height -= self.messageText.frame.height + self.toolbar.frame.height
            var customheight = self.messageText.frame.height + self.toolbar.frame.height + rect.height/2
            var x = self.ChatView.frame.height - customheight
            
            var tableFrame = CGRectMake(self.ChatView.frame.origin.x, self.ChatView.frame.origin.y, self.ChatView.frame.width,x )
            self.ChatView.frame = tableFrame
            self.messageText.frame = frame
            self.toolbar.frame = toolbarFrame
            self.toolbar.multipleTouchEnabled = true
            self.image.frame = imageFrame
            var imageFrame = CGRectMake(self.image.frame.origin.x, self.image.frame.origin.y, self.image.frame.width, self.image.frame.height)
            
        })
        UIView.commitAnimations()
        self.ChatView.scrollBubbleViewToBottomAnimated(true)
        
    }
    
    func keyboardDidHide(notification : NSNotification){
        print("\(self.messageText.frame.origin)")
        var c :NSDictionary = notification.userInfo! // as NSDictionary
        
        var point:NSValue = NSValue(nonretainedObject: c.objectForKey(UIKeyboardFrameEndUserInfoKey))
        //var keyboardSize = let dict:NSDictionary = sender.userInfo as NSDictionary
        let s:NSValue = c.valueForKey(UIKeyboardFrameEndUserInfoKey) as NSValue;
        let rect :CGRect = s.CGRectValue();
        
        
        var frame = self.messageText.frame
        var toolbarFrame = self.toolbar.frame
        // var textFrame = self.textfieldFrame.frame
        var imageFrame = self.image.frame
        var offset = (rect.height - ((self.view.frame.height - self.messageText.frame.origin.y) + self.messageText.frame.size.height)) + 80
        frame.origin.y = self.messageText.frame.origin.y + rect.height
        toolbarFrame.origin.y = self.toolbar.frame.origin.y + rect.height
        imageFrame.origin.y = self.image.frame.origin.y + rect.height
        //textFrame.origin.y = self.textfieldFrame.frame.origin.y - rect.height
        UIView.animateWithDuration(0.1, animations: {
            //            self.textfieldFrame.frame = textFrame
            
            //self.ChatView.frame.height -= self.messageText.frame.height + self.toolbar.frame.height
            var customheight = self.messageText.frame.height + self.toolbar.frame.height + rect.height/2
            var x = self.ChatView.frame.height + customheight
            
            var tableFrame = CGRectMake(self.ChatView.frame.origin.x, self.ChatView.frame.origin.y, self.ChatView.frame.width,x )
            self.ChatView.frame = tableFrame
            self.messageText.frame = frame
            self.toolbar.frame = toolbarFrame
            self.toolbar.multipleTouchEnabled = true
            self.image.frame = imageFrame
            
        })
        UIView.commitAnimations()
        
        self.ChatView.scrollBubbleViewToBottomAnimated(true)
        
        
    }
    
    func rowsForBubbleTable(tableView: UIBubbleTableView!) -> Int {
        return bubbleData.count
    }
    func bubbleTableView(tableView: UIBubbleTableView!, dataForRow row: Int) -> NSBubbleData! {
        self.ChatView.scrollBubbleViewToBottomAnimated(true)
        var obj = bubbleData.objectAtIndex(row) as NSBubbleData
        //println("\(messages   ) este vectorul")
        return bubbleData.objectAtIndex(row) as NSBubbleData
        
    }
    func initNetworkCommunication() {
        
        //declari niste stream-uri din care citesti si in care  scrii
        var readStream:Unmanaged<CFReadStream>?;
        var writeStream:Unmanaged<CFWriteStream>?     ;
        //localhost = 86.120.239.5
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,"localhost",8023,&readStream,&writeStream)
        
        inputStream = readStream!.takeUnretainedValue()
        outputStream = writeStream!.takeUnretainedValue()
        
        self.inputStream!.delegate=self
        self.outputStream!.delegate = self
        
        self.inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        //println("\(UsernameField.text)");
        var msg = "iam:\(self.nameofSender).thirdView\r\n"
        //println("\(msg)")
        var ptr = msg.nulTerminatedUTF8
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
      
    }
    
    func stream(theStream:NSStream , handleEvent streamEvent: NSStreamEvent) {
        switch(streamEvent){
        case NSStreamEvent.OpenCompleted :
            println("Opened correctly")
        case NSStreamEvent.HasBytesAvailable :
            if (theStream == inputStream){
                // println("macar intra aici pe receive")
                var len : Int
                var buffer : UnsafePointer<uint_fast8_t>!
                while (inputStream.hasBytesAvailable){
                    let buffer2=UnsafeMutablePointer<UInt8>.alloc(1000);
                    len = inputStream.read(buffer2, maxLength: 1000)
                    if(len>0){
                        var output :NSString = NSString(bytes: buffer2, length: len, encoding: NSASCIIStringEncoding)
                        //aici trebuie sa faci si tu un split sa vezi daca e numele tau cel de la care a trimis
                        //si daca e sa afisezi cu alta culoare si vezi daca nu e numele tau afisezi c u alta culoare
                        
                        //REMINDER: nu uita de treaba aia cu variabila dintr-un controller in altul
                        
                        if output != "" {
                            
                            println("Server said : \(output)")
                            self.messageReceived(output)
                            
                            println(" \(messages)  ")
                            ChatView.reloadData()
                            
                        }
                    }
                }
                
            }
            
        case NSStreamEvent.ErrorOccurred :
            println("Can not connect to the host")
        case NSStreamEvent.EndEncountered :
            println("Here it is")
            
        default:
            println("Unknown event")
        }
    }
    
    func showMessage(notification : NSNotification){
        var ChatView    : ViewController3
        //nu lasa asa pune sa afiseze ceview vri tu
        
        ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView") as ViewController3
        ChatView.nameofReceiver = self.futurePerson
        ChatView.nameofSender = self.nameofSender
        self.inputStream.close()
        self.outputStream.close()
        self.presentViewController(ChatView, animated: true, completion: nil)
        
    }
    
    
    
    
    
    func messageReceived(msg:NSString){
        var split :NSArray
        var text = msg
        var banner : ALAlertBanner!
        
        split = msg.componentsSeparatedByString(":")
        var ceva :NSString
        ceva = msg
        // text = split.objectAtIndex(1) as NSString
        // var ceva = String()
        if msg.containsString("has joined"){
            
        }
        else if msg.containsString("conversation saved"){
            println("Intra aici")
            self.inputStream.close()
            self.outputStream.close()
            var UsersView :ViewController2
            UsersView = self.storyboard?.instantiateViewControllerWithIdentifier("SecondView") as ViewController2
            UsersView.name = self.nameofSender
            self.messages.removeAllObjects()
            self.viewWillDisappear(true)
            self.presentViewController(UsersView, animated: true, completion:nil)
            
            
        }
        else if msg.containsString("Conversation:"){
            println("aici nu cred ca intra si mesajul este \(msg) dsa das")
            var messagesNotFormatted = msg.componentsSeparatedByString("Conversation:")[1] as NSString
            if !msg.containsString("no such file"){
                var fullLine = messagesNotFormatted.componentsSeparatedByString("}\n")
            
                for line in fullLine {
                    
                    var wholeMessage = line.componentsSeparatedByString("{")[1] as NSString
                    if wholeMessage.containsString("me on server"){
                        
                        var message = wholeMessage.componentsSeparatedByString("me on server")[0] as NSString
                        //println("\(message ) is the message i parsed")
                        var paths2 :NSArray = NSSearchPathForDirectoriesInDomains(.DocumentDirectory , .UserDomainMask, true)
                        var docsDirectory :NSString = paths2.objectAtIndex(0) as NSString
                        var pathing = docsDirectory.stringByAppendingPathComponent("test.png")
                        var imageFromTable = UIImage(contentsOfFile: pathing)
                        
                        var dataFromMe = NSBubbleData(text: "\(message)", date: NSDate(timeIntervalSinceNow: 0), type: BubbleTypeMine)
                        dataFromMe.avatar = imageFromTable
                        var date = NSDate(timeIntervalSinceNow: 0)
                        bubbleData[pos] = dataFromMe
                        messages[pos] = "{" + message + " me on server}"
                        
                        pos++
                    }
                    else if wholeMessage.containsString("him on server"){
                        var message2 = wholeMessage.componentsSeparatedByString("him on server")[0] as NSString
                        var dataFromMe = NSBubbleData(text: "\(message2)", date: NSDate(timeIntervalSinceNow: 0), type: BubbleTypeSomeoneElse
                        )
                     
                        var date = NSDate(timeIntervalSinceNow: 0)
                        bubbleData[pos] = dataFromMe
                        messages[pos] = "{" + message2 + " him on server}"
                        
                        
                        pos++
                    }
                    
                    
                    
                }
                self.ChatView.reloadData()
                self.ChatView.scrollBubbleViewToBottomAnimated(true)
            }
            
        }
        else if msg.containsString("You just sent a message"){
            
        }
        else if msg.containsString("you have a message") {
            var fromuser = msg.componentsSeparatedByString("you have a message")[1].componentsSeparatedByString(":")[2] as NSString
            //println("asdasd ads asd\(fromuser)")
            if fromuser == self.receiverLabel.text  {
                var partOfIt = msg.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[6] as NSString
                futurePerson = fromuser
                var localnotification:UILocalNotification = UILocalNotification()
                localnotification.category = "FIRST_CATEGORY"
                localnotification.alertBody = "\(fromuser) said: \(partOfIt)"
                localnotification.fireDate = NSDate(timeIntervalSinceNow: 5)
                UIApplication.sharedApplication().scheduleLocalNotification(localnotification)
                //println("asdasd ads asd\(fromuser)")
                var ceva = "\(partOfIt)"
                var datafromUser = NSBubbleData(text: "\(ceva)", date: NSDate(timeIntervalSinceNow: 0) , type: BubbleTypeSomeoneElse)
                var date = NSDate(timeIntervalSinceNow: 0)
                bubbleData[pos] = datafromUser
                messages[pos] = "{" + ceva + " him on server}"
                pos++
            }
            else{
                var partOfIt = msg.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[6] as NSString
                var tuple = (partOfIt,fromuser)
                println("\(self.nameofReceiver) \(self.nameofSender)    este numele lor ")
                allertManager.alertBannersInView(self.view)
                // messagefrom.append(tuple)
                //BRO DO SOMETHING ABOUT THIS IMMUTABLE THING RIGHT HERE-12.08.2014
                //println("Deschide un view nou")
                
                
                //banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "Success",subtitle:"Ce mai faci",tappedBlock:
                
                
                //daca apasa ar trebui sa afisezi un alt view
                banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "Message from \(fromuser) : ",subtitle: partOfIt , tappedBlock:{ (banner :ALAlertBanner!) in
                    println("Aici intra?")
                    var ChatView    : ViewController3
                    //nu lasa asa pune sa afiseze ceview vri tu
                    
                    ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView") as ViewController3
                    ChatView.nameofReceiver = fromuser
                    ChatView.nameofSender = self.nameofSender
                    
                    
                    self.inputStream.close()
                    self.outputStream.close()
                    
                    var date = NSDate(timeIntervalSinceNow: 2)
                    var ceva2 = date.timeIntervalSinceDate(date)
                    banner.secondsToShow = ceva2
                    // self.viewWillDisappear(true)
                    
                    self.presentViewController(ChatView, animated: true, completion:nil)
                    
                    self.allertManager.hideAlertBannersInView(self.view)
                    banner.hide()
                })
                
                //banner.exclusiveTouch = true
                //)
                //println("YOU HAVE TAPPED IT")
                //self.view2.addSubview(banner)
                banner.multipleTouchEnabled = true
                banner.show()
                
                
            }
        }
        else if msg.containsString("not online"){
            allertManager.alertBannersInView(self.view)
            var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleNotify, position: ALAlertBannerPositionTop, title: "user has disconnected")
            banner.show()
        }
        else if msg.containsString("user online"){
            
        }
        else{
            NSBubbleData(image: UIImage(named: "globe"), date: NSDate(timeIntervalSinceNow: 0), type: BubbleTypeMine)
            var dataFromMe = NSBubbleData(text: "\(ceva)", date: NSDate(timeIntervalSinceNow: 0), type: BubbleTypeMine)
            var date = NSDate(timeIntervalSinceNow: 0)
            bubbleData[pos] = dataFromMe
            messages[pos] = "{" + ceva + " him on server}";
            pos++
        }
        
    }
    
    
    @IBOutlet var image: UIImageView!
    
    
    func textViewDidEndEditing(textView: UITextView) {
        println("no moreediting")
        ChatView.typingBubble = NSBubbleTypingTypeNobody
        ChatView.reloadData()
    }
    func textViewDidBeginEditing(textView: UITextView) {
        println("you clicked ")
        
        ChatView.typingBubble = NSBubbleTypingTypeMe
        self.ChatView.reloadData()
        var image = UIImageView()
        //UIImage(
        image.image = UIImage(named:"tip")
        image.frame = CGRectMake(self.image.frame.origin.x, self.image.frame.origin.y, self.image.frame.width, self.image.frame.height * 2)
        //self.view.addSubview(image)
        self.image.frame = CGRectMake(self.image.frame.origin.x, self.image.frame.origin.y, self.image.frame.width, self.image.frame.height * 2)
    }
    
    
    
    
    @IBAction func goBack (sender : UIBarButtonItem!){
        var UsersView    : ViewController2
        
        
        UsersView = self.storyboard?.instantiateViewControllerWithIdentifier("SecondView") as ViewController2
        UsersView.name = self.nameofSender
        var string : NSString
        println("\(messages) sunt mesajele")
        string = messages.componentsJoinedByString("\n")
        var msg = "saveconversation:\(self.nameofSender):\(self.nameofReceiver):\(string):ds"
        self.outputStream.write(msg, maxLength :msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
 
        
        
    }
    
    @IBAction func sendMessage(sender: UIBarButtonItem!){
        
        //ai grija ca aici ar trebui sa trimiti atat la nume cat si nume.thirdview si tot asa vezi sa implementezi ceva cu
        //getusers si tot asa
        var msg = self.messageText.text as String!
        var uname = self.nameofSender as String!
        var hname = self.nameofReceiver as String!
        self.messageText.endEditing(true)
        var response = "msgtouser:"
        response += uname!
        response += ":"
        response += hname!
        response += ":" + msg + ":dsasd "
        
        var res : Int
        self.outputStream.write(response, maxLength :response.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
        
        response = "msgtouser:"
        response += uname!
        response += ":"
        response += hname! + ".thirdView\r\n"
        response += ":" + msg + ":dsasd "
        self.outputStream.write(response, maxLength :response.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        response = "msgtouser:"
        response += uname!
        response += ":"
        response += hname! + ".mapView\r\n"
        response += ":" + msg + ":dsasd "
        self.outputStream.write(response, maxLength :response.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
        response = "msgtouser:"
        response += uname!
        response += ":"
        response += hname! + ".secView\r\n"
        response += ":" + msg + ":dsasd "
        self.outputStream.write(response, maxLength :response.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        response = "msgtouser:"
        response += uname!
        response += ":"
        response += hname! + ".groupView\r\n"
        response += ":" + msg + ":dsasd "
        self.outputStream.write(response, maxLength :response.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        response = "msgtouser:"
        response += uname!
        response += ":"
        response += hname! + ".editInfo\r\n"
        response += ":" + msg + ":dsasd "
        self.outputStream.write(response, maxLength :response.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        response = "msgtouser:"
        response += uname!
        response += ":"
        response += hname! + ".friendView\r\n"
        response += ":" + msg + ":dsasd "
        self.outputStream.write(response, maxLength :response.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
        self.messageText.text = ""
        var UsersView    : ViewController2
        self.ChatView.scrollBubbleViewToBottomAnimated(true)
        
        var paths2 :NSArray = NSSearchPathForDirectoriesInDomains(.DocumentDirectory , .UserDomainMask, true)
        var docsDirectory :NSString = paths2.objectAtIndex(0) as NSString
        var pathing = docsDirectory.stringByAppendingPathComponent("test.png")
        var imageFromTable = UIImage(contentsOfFile: pathing)
        
        var ceva = "ALTA BUCURIE"
        var partOfIt = msg
        ceva = "\(partOfIt)"
        
        var dataFromMe = NSBubbleData(text: "\(ceva)", date: NSDate(timeIntervalSinceNow: 0), type: BubbleTypeMine)
        dataFromMe.avatar = imageFromTable
        var date = NSDate(timeIntervalSinceNow: 0)
        bubbleData[pos] = dataFromMe
        messages[pos] = "{" + ceva + " me on server}"
        pos++
    }
    func sendMessage2(){
        var uname = self.nameofSender as String!
        var hname = self.nameofReceiver as String!
        
        var res = "isonline:\(hname):dsa"
        self.outputStream.write(res, maxLength :res.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
    }
    func sendMessage3(){
        var msg = "getconversation:\(self.nameofSender):\(self.nameofReceiver):ds"
        var res = self.outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
    }
    
}