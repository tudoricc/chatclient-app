//
//  TableViewController.swift
//  ChatClien
//
//  Created by Tudor on 26/08/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    @IBOutlet var ContactName: UILabel!
    
    @IBOutlet var ContactImage: UIImageView!
    
    
    @IBOutlet var ContactDetails: UITableView!
    
    
    var contactInfo : NSDictionary! = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // ContactDetails.delegate = self
        
        self.populateContactData()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func populateContactData(){
        var fname = contactInfo.objectForKey("firstName") as NSString
        var lname = contactInfo.objectForKey("lastName") as NSString
        var fullName = "\(fname) \(lname) "
        self.ContactName.text = fullName
        
        if self.contactInfo.objectForKey("image") != nil {
            ContactImage.image = self.contactInfo.objectForKey("image") as UIImage
        }
        
        ContactDetails!.reloadData()
        
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 3
    }
    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if section == 0 {
            return 2
        }
        else if section == 1 {
            return 2
        }
        else {
            return 3
            
        }
    }
    override func tableView(tableView: UITableView!,
        titleForHeaderInSection section: Int) -> String!{
            switch (section) {
            case 0 :
                return "Phone number"
            case 1 :
                return "E-mail address"
            case 2 :
                return "Address Info"
            default :
                return ""
            }
    }
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell!{
        var cell :UITableViewCell = self.ContactDetails.dequeueReusableCellWithIdentifier("DetailCell") as UITableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "DetailCell")
        }
        var cellText = ""
        var detailText = ""
        var x = ""
        switch (indexPath.section) {
        case 0 :
            switch indexPath.row{
            case 0 :
                cellText = contactInfo.objectForKey("mobileNumber") as NSString
                detailText = "Mobile Number"
            default:
                x = ""
                
            }
        case 1 :
            switch indexPath.row{
            case 0 :
                cellText = contactInfo.objectForKey("homeEmail") as NSString
                detailText = "Home Email"
            case 1 :
                cellText = contactInfo.objectForKey("workEmail") as NSString
                detailText = "Work Email"
            default :
                x = ""
                
                
            }
        case 2 :
            switch indexPath.row {
            case 0:
                cellText = contactInfo.objectForKey("address") as NSString
                detailText = "Street Adress"
            case 1:
                cellText = contactInfo.objectForKey("zip") as NSString
                detailText = "Zip Code"
            case 2:
                cellText = contactInfo.objectForKey("city") as NSString
                detailText = "City"
            default :
                x = ""
            }
        default:
            x = ""
        }
        
        
        cell.textLabel.text = cellText
        cell.detailTextLabel.text = detailText
        return cell
    }
    
    
    func setDictContactDetails(newDict :NSDictionary) {
        self.contactInfo = newDict
    }

       /*
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
