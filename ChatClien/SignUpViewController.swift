//
//  SignUpViewController.swift
//  ChatClien
//
//  Created by Tudor on 27/08/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,NSStreamDelegate,UITextFieldDelegate{

    @IBOutlet var welcomeLabel: UILabel!
    @IBOutlet var phoneBar: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var nameField: UITextField!
    
    var inputStream : NSInputStream!
    var outputStrean : NSOutputStream!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameField.delegate = self
        self.passwordField.delegate = self
        self.emailField.delegate = self
        self.phoneBar.delegate = self
        //self.nameField.addTarget(delegate, action: "touched:", forControlEvents: UIControlEvents.EditingDidBegin)
        self.welcomeLabel.text = "Registration"
        self.welcomeLabel.font = UIFont.boldSystemFontOfSize(welcomeLabel.font.pointSize)
        
        self.welcomeLabel.textColor = UIColor.darkGrayColor()
        self.welcomeLabel.shadowColor = UIColor.whiteColor()
        self.welcomeLabel.shadowOffset = CGSizeMake(0, 10)
        self.passwordField.secureTextEntry = true
        
        self.initNetworkCommunication()
       
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldDidBeginEditing(textField: UITextField!) {
        if textField.font == UIFont.italicSystemFontOfSize(textField.font.pointSize){
        textField.text = ""
        textField.font = UIFont.boldSystemFontOfSize(textField.font.pointSize)
        }
        else{
        if textField == phoneBar {
            var beginning = textField.beginningOfDocument
            var end = textField.endOfDocument
            textField.selectedTextRange  = textField.textRangeFromPosition(beginning, toPosition: end)
        }
        }
    }
    func initNetworkCommunication() {
        
        //declari niste stream-uri din care citesti si in care  scrii
        var readStream:Unmanaged<CFReadStream>?;
        var writeStream:Unmanaged<CFWriteStream>?     ;
        
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,"localhost",8023,&readStream,&writeStream)
        
        inputStream = readStream!.takeUnretainedValue()
        outputStrean = writeStream!.takeUnretainedValue()
        
        self.inputStream!.delegate=self
        self.outputStrean!.delegate = self
        
        self.inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStrean!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStrean.open()
        
        let myVar : Int = Int(rand())
        var msg = "addme:\(myVar)\r\n"
        
        var ptr = msg.nulTerminatedUTF8
        var res = outputStrean.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
        
       
        
    }

    
    func stream(theStream:NSStream , handleEvent streamEvent: NSStreamEvent) {
        switch(streamEvent){
        case NSStreamEvent.OpenCompleted :
            println("Opened correctly")
        case NSStreamEvent.HasBytesAvailable :
            if (theStream == inputStream){
                
                var len : Int
                var buffer : UnsafePointer<uint_fast8_t>!
                while (inputStream.hasBytesAvailable){
                    let buffer2=UnsafeMutablePointer<UInt8>.alloc(12230);
                    len = inputStream.read(buffer2, maxLength: 12230)
                    if(len>0){
                        var output :NSString = NSString(bytes: buffer2, length: len, encoding: NSASCIIStringEncoding)
                        //aici trebuie sa faci si tu un split sa vezi daca e numele tau cel de la care a trimis
                        //si daca e sa afisezi cu alta culoare si vezi daca nu e numele tau afisezi c u alta culoare
                        
                        //REMINDER: nu uita de treaba aia cu variabila dintr-un controller in altul
                        if ( output == ""){
                            
                        }
                        else{
                             println("Server said : \(output)")
                            self.messageReceived(output)
                        }
                    }
                }
                
            }
        case NSStreamEvent.ErrorOccurred :
            println("Can not connect to the host")
        case NSStreamEvent.EndEncountered :
            println("The server has closed for some issues or your internet connection is down")
        case NSStreamEvent.HasSpaceAvailable :
            println("You received somthing")
        default:
            println("Unknown event")
        }
    }
    
    
    
    func messageReceived(msg:NSString){
        var split :NSArray
        var text = msg
        
        split = msg.componentsSeparatedByString(":")
        var ceva = msg
    
    
    
    
        if msg.containsString("you were successfully added") {
           var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "You were succesfully added",subtitle: "Click to log in" , tappedBlock:{ (banner :ALAlertBanner!) in
                //println("Aici intra?")
               var allertManager : ALAlertBannerManager = ALAlertBannerManager()
                allertManager.alertBannersInView(self.view)

                var MainView : ViewController
                MainView = self.storyboard?.instantiateViewControllerWithIdentifier("MainView") as ViewController
            MainView.name = self.nameField.text
            
                self.inputStream.close()
                self.outputStrean.close()
                
                var date = NSDate(timeIntervalSinceNow: 5)
            
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
            
            
                self.viewWillDisappear(true)
                self.presentViewController(MainView, animated: true, completion: nil)
                //self.presentViewController(ChatView, animated: true, completion:nil)
                
                allertManager.hideAlertBannersInView(self.view)
                banner.hide()
                 })
            banner.multipleTouchEnabled = true
            banner.show()
            
            
        }
        else if msg.containsString("you weren't successfully added"){
            //we werent' successfully added
            
            
            
            //
            //we do this just to notify the user that something went wrong
            //so we show some fields in red.
            self.nameField.textColor = UIColor.redColor()
            self.emailField.textColor = UIColor.redColor()
            self.phoneBar.textColor = UIColor.redColor()
            self.nameField.text = self.nameField.text
            self.nameField.font = UIFont.boldSystemFontOfSize(self.nameField.font.pointSize)
            self.nameField.textColor = UIColor.redColor()
            self.emailField.text  = self.emailField.text
            self.emailField.textColor = UIColor.redColor()
            self.emailField.font = UIFont.boldSystemFontOfSize(self.emailField.font.pointSize)
            self.passwordField.text = ""
            self.phoneBar.text = self.phoneBar.text
            self.phoneBar.textColor = UIColor.redColor()
            self.phoneBar.font = UIFont.boldSystemFontOfSize(self.phoneBar.font.pointSize)

            //delete the content of the password field
            self.passwordField.text = ""
            
            
            //bold the text inside some fields
            self.nameField.font = UIFont.boldSystemFontOfSize(self.nameField.font.pointSize)
            self.emailField.font = UIFont.boldSystemFontOfSize(self.emailField.font.pointSize)
            self.phoneBar.font = UIFont.boldSystemFontOfSize(self.phoneBar.font.pointSize)
            
            
            var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure , position: ALAlertBannerPositionTop, title: "You weren't succesfully added",subtitle: "There is another person with the same name / email / phone number" , tappedBlock:{ (banner :ALAlertBanner!) in
                //println("Aici intra?")
                var allertManager : ALAlertBannerManager = ALAlertBannerManager()
                allertManager.alertBannersInView(self.view)
                
                var ThisView : SignUpViewController
                ThisView = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpView") as SignUpViewController
                
                
                self.inputStream.close()
                self.outputStrean.close()
                
                var date = NSDate(timeIntervalSinceNow: 3)
                
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                
                
                self.viewWillDisappear(true)
                self.presentViewController(ThisView, animated: true, completion: nil)
                //self.presentViewController(ChatView, animated: true, completion:nil)
                
                allertManager.hideAlertBannersInView(self.view)
                banner.hide()
            })
            banner.multipleTouchEnabled = true
            banner.show()
            

        }
        else{
            //we really do nothing here becaue the server is telling us just that we have joined
            
        }
    
    }
    
    @IBAction func SubmitRequest(sender: UIBarButtonItem) {
        //here you  make a request to the server where you send him a request to add you.
        //for now we'll just make something to send a request to be added to the server and 
        //depending on what the server says we'll decide if we added him or not
       var email = self.emailField.text
        var nsstr : NSString = NSString(string: email)
        var pattern = "^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$"
        var number = self.phoneBar.text.toInt()
        println("\(number)")
        //println("\(self.passwordField.text)")
        if (self.passwordField.text.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) > 8 && nsstr.containsString("@")  && number? != nil && number! > 100000000  && (nsstr.containsString(".com") || nsstr.containsString(".org")) && self.nameField.text.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) > 5 && self.emailField.text.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) > 7 ){
           // println("Intra aici")
            var msg = "signup:\(self.nameField.text):\(self.emailField.text):\(self.passwordField.text):\(self.phoneBar.text):dsadas "
            var res = outputStrean.write(msg, maxLength: msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        }
        else if (number == nil || number < 100000000) {
            var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure , position: ALAlertBannerPositionTop, title: "Phone Number",subtitle: "Please enter a valid Phone number" , tappedBlock:{ (banner :ALAlertBanner!) in
                var date = NSDate(timeIntervalSinceNow: 3)
                 self.phoneBar.textColor = UIColor.redColor()
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                banner.hide()
                self.phoneBar.text = ""
               
            })
            banner.show()
        }
        else if (self.nameField.text.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) <= 5){
            var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure , position: ALAlertBannerPositionTop, title: "Name",subtitle: "Please enter a longer name" , tappedBlock:{ (banner :ALAlertBanner!) in
                var date = NSDate(timeIntervalSinceNow: 3)
                
                self.emailField.textColor = UIColor.redColor()
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                banner.hide()
            })
            banner.show()
        }
        else if (self.passwordField.text.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) <= 8 ){
            var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure , position: ALAlertBannerPositionTop, title: "Password",subtitle: "Your password is too short" , tappedBlock:{ (banner :ALAlertBanner!) in
                var date = NSDate(timeIntervalSinceNow: 3)
                
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                banner.hide()
                self.passwordField.text = ""
            })
            banner.show()
        }

        else if (nsstr.length <= 7 || !nsstr.containsString(".com") || !nsstr.containsString(".org")){
            var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure , position: ALAlertBannerPositionTop, title: "Email",subtitle: "Please enter a valid Email Address" , tappedBlock:{ (banner :ALAlertBanner!) in
                var date = NSDate(timeIntervalSinceNow: 3)
                
                self.emailField.textColor = UIColor.redColor()
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                banner.hide()
            })
            banner.show()
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
     
        if segue.identifier == "goBack"
        {
            self.inputStream.close()
            self.outputStrean.close()
        }
    }
   
}
