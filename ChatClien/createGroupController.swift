//
//  createGroupController.swift
//  ChatClien
//
//  Created by Tudor on 17/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit

class createGroupController: UIViewController,UITableViewDelegate,UITableViewDataSource,NSStreamDelegate{
    
    @IBOutlet var inputField: UITextField!
    @IBOutlet var label: UILabel!
    
    
    @IBOutlet var label2: UILabel!
    
    @IBOutlet var label3: UILabel!
    @IBOutlet var table: UITableView!
    
    var messages : NSMutableArray = NSMutableArray()
    var inputStream :NSInputStream!
    var outputStream :NSOutputStream!
    var name:NSString!
    var pos = 0
    override func viewDidLoad() {
        table.delegate = self
        table.dataSource = self
        table.hidden = true

        self.initNetworkCommunication()
        
        
        var x = 1
        while x<3{
        NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "sendMessage2", userInfo: nil, repeats: true)
        table.reloadData()
         x++
        }
       // messages.removeAllObjects()
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SelectPage(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            table.hidden = true
            label3.hidden = true
            label.hidden = false
            label2.hidden = false
            inputField.hidden = false
        }
        else {
            label.hidden = true
            label2.hidden = true
            inputField.hidden = true
            table.hidden = false
            label3.hidden = false
            
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cellIdentifier = "Cell2"
        var cell :UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as UITableViewCell
        var item = messages[indexPath.row] as NSString
//        cell.groupName.text = item
        cell.alpha = 0.5
        cell.textLabel?.text = item
       // table.reloadData()
        return cell
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func initNetworkCommunication() {
        
        
        //  declari niste stream-uri din care citesti si in care  scrii
        var readStream:Unmanaged<CFReadStream>?;
        var writeStream:Unmanaged<CFWriteStream>?     ;
        //localhost = 86.120.239.5
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,"localhost",8023,&readStream,&writeStream)
        
        inputStream = readStream!.takeUnretainedValue()
        outputStream = writeStream!.takeUnretainedValue()
        
        self.inputStream!.delegate=self
        self.outputStream!.delegate = self
        
        self.inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        
        //println("\(UsernameField.text)");
        var msg = "iam:\(self.name).groupView\r\n"
        
        //self.myname = msg.componentsSeparatedByString(":")[1] as NSString
        //println("\(msg)")
        var ptr = msg.nulTerminatedUTF8
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
        
    }
    
    
    func stream(theStream:NSStream , handleEvent streamEvent: NSStreamEvent) {
        switch(streamEvent){
        case NSStreamEvent.OpenCompleted :
            
            var ceva = ""
        case NSStreamEvent.HasBytesAvailable :
            if (theStream == inputStream){
                
                var len : Int
                var buffer : UnsafePointer<uint_fast8_t>!
                while (inputStream.hasBytesAvailable){
                    let buffer2=UnsafeMutablePointer<UInt8>.alloc(12230);
                    len = inputStream.read(buffer2, maxLength: 12230)
                    if(len>0){
                        var output :NSString = NSString(bytes: buffer2, length: len, encoding: NSASCIIStringEncoding)
                        
                        if ( output == ""){
                            
                        }
                        else{
                            if output.containsString("has joined"){
                                println("-------------->you have joined<-------------")
                            }
                            else if output.containsString("available groups"){
                                //println("Intra aici")
                                var smth = output.componentsSeparatedByString("\n")[1] as NSString
                                //println("groups are : \(smth)")
                                var smthelse = smth.componentsSeparatedByString("groups:")[1] as NSString
                                var listOfGroups = smthelse.componentsSeparatedByString(":")
                               // println("only the groups are : \(listOfGroups)")
                                for group in listOfGroups {
                                    var cuvant :NSString = "\(group)"
                                    var candy = Candy(category: "All", name: cuvant)
                                    if cuvant.containsString("available"){
                                        var x = cuvant.componentsSeparatedByString("available")[0] as NSString
                                        self.messages[pos] = x
                                        pos++
                                    }
                                    else{
                                    self.messages[pos] = cuvant
                                    pos++
                                    }
                                }
                                self.table.reloadData()
                                pos = 0
                            }
                            else if output.containsString("You are now part of the group"){
                                var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess , position: ALAlertBannerPositionTop, title: "Success",subtitle: "Congratulations on creating a new group" , tappedBlock:{ (banner :ALAlertBanner!) in
                                    //var date = NSDate(timeIntervalSinceNow: 3)
                                    //self.phoneBar.textColor = UIColor.redColor()
                                   // var ceva2 = date.timeIntervalSinceDate(date)
                                   // banner.secondsToShow = ceva2
                                    banner.hide()
                                    // self.phoneBar.text = ""
                                    
                                })
                                banner.exclusiveTouch = true
                                banner.show()
                            }
                            else if output.containsString("we have an error"){
                                var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess , position: ALAlertBannerPositionTop, title: "Success",subtitle: "Congratulations on creating a new group" , tappedBlock:{ (banner :ALAlertBanner!) in
                                    var date = NSDate(timeIntervalSinceNow: 3)
                                    //self.phoneBar.textColor = UIColor.redColor()
                                    var ceva2 = date.timeIntervalSinceDate(date)
                                    banner.secondsToShow = ceva2
                                    banner.hide()
                                    // self.phoneBar.text = ""
                                    
                                })
                                banner.show()

                            }
                            else if output.containsString("you have a message") {
                                println("YOU HAVE A MESSAGE")
                                var fromuser =  output.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[2] as NSString
                                var onlythemessage = output.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[6] as NSString
                                var banner :ALAlertBanner
                                banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "message from \(fromuser)", subtitle: onlythemessage, tappedBlock: { (banner :ALAlertBanner!) in
                                    var ChatView    : ViewController3
                                    //nu lasa asa pune sa afiseze ceview vri tu
                                    
                                    ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView") as ViewController3
                                    ChatView.nameofReceiver = fromuser
                                    ChatView.nameofSender = self.name
                                    
                                    self.inputStream.close()
                                    self.outputStream.close()
                                    self.viewWillDisappear(true)
                                    self.presentViewController(ChatView, animated: true, completion:nil)
                                })
                                banner.exclusiveTouch = true
                                banner.show()
                                var localnotification:UILocalNotification = UILocalNotification()
                                localnotification.category = "FIRST_CATEGORY"
                                localnotification.alertBody = "\(fromuser) said: \(onlythemessage)"
                                localnotification.fireDate = NSDate(timeIntervalSinceNow: 5)
                                UIApplication.sharedApplication().scheduleLocalNotification(localnotification)

                            }
                            else {
                                println("\(output) sad sda asd as ")
                            }
                            
                        }
                    }
                }
                
            }
        case NSStreamEvent.ErrorOccurred :
            var x = 1
        case NSStreamEvent.EndEncountered :
            
            var connectionAlert = UIAlertView(title: "Connection Error", message: "The server is down you can not do anything in this mode", delegate: self, cancelButtonTitle: "Close")
            self.view.addSubview(connectionAlert)
            connectionAlert.show()
        default:
            var ceva = ""
            
        }
    }
    func sendMessage2(){
        var msg = "getgroups:\(self.name):sdasa"
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        table.reloadData()
        pos = 0
    }
    
    @IBAction func submitRequest(sender: AnyObject) {
        var groupName = self.inputField.text as NSString
        if messages.containsObject(groupName){
            println("Il are deja")
            var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure , position: ALAlertBannerPositionTop, title: "Error",subtitle: "There is another group with this name" , tappedBlock:{ (banner :ALAlertBanner!) in
                //var date = NSDate(timeIntervalSinceNow: 3)
                //self.phoneBar.textColor = UIColor.redColor()
                //var ceva2 = date.timeIntervalSinceDate(date)
                //banner.secondsToShow = ceva2
                banner.hide()
               // self.phoneBar.text = ""
                
            })
            banner.exclusiveTouch = true
            banner.show()
        }
        else if groupName.length < 5{
            var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure , position: ALAlertBannerPositionTop, title: "Error",subtitle: "Group Name is too short" , tappedBlock:{ (banner :ALAlertBanner!) in
                var date = NSDate(timeIntervalSinceNow: 3)
                //self.phoneBar.textColor = UIColor.redColor()
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                banner.hide()
                // self.phoneBar.text = ""
                
            })
            banner.show()
        }
        else if groupName == ""{
            var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure , position: ALAlertBannerPositionTop, title: "Error",subtitle: "Group's name should not be empty" , tappedBlock:{ (banner :ALAlertBanner!) in
                var date = NSDate(timeIntervalSinceNow: 3)
                //self.phoneBar.textColor = UIColor.redColor()
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                banner.hide()
                // self.phoneBar.text = ""
                
            })
            banner.show()
        }
        else{
            var msg = "makegroup:\(groupName):\(self.name):dsa"
            var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))

        }
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goBack" {
            var dest = segue.destinationViewController as GroupsViewController
            dest.name = self.name
            self.inputStream.close()
            self.outputStream.close()
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
