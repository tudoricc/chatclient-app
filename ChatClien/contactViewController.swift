//
//  NewViewController.swift
//  ChatClien
//
//  Created by Tudor on 26/08/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit
import MessageUI

class NewViewController: UIViewController , UITableViewDelegate , UIActionSheetDelegate , MFMessageComposeViewControllerDelegate{
    @IBOutlet var userTable: UITableView!
    @IBOutlet var userinoImagerino: UIImageView!
    @IBOutlet var nameOfUser: UILabel!
    

    override func viewDidLoad() {
      //  userTable.delegate = self
        
        super.viewDidLoad()
//       println("\(self.contactInfo)IS TEH ")
//            println(self.contactInfo.objectForKey("lastName"))
//        println(self.contactInfo.objectForKey("firstName"))
        var fname = contactInfo.objectForKey("firstName") as NSString
        var lname = contactInfo.objectForKey("lastName") as NSString
       var fullName = "\(fname) \(lname) "
//        
        self.nameOfUser.text = fullName
    
        //  self.nameOfUser.text = fullName
        
        if self.contactInfo.objectForKey("image") as NSString != "" {
           userinoImagerino.image = self.contactInfo.objectForKey("image") as? UIImage
        }
        
        userTable?.reloadData()
        //self.populateContactData()
        //self.userTable.registerClass(<#aClass: AnyClass!#>, forHeaderFooterViewReuseIdentifier: <#String!#>)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var contactInfo : NSMutableDictionary!

    
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 3
    }
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if section == 0 {
            return 2
        }
        else if section == 1 {
            return 2
        }
        else {
            return 3
            
        }
    }
    func tableView(tableView: UITableView!,
        titleForHeaderInSection section: Int) -> String!{
            switch (section) {
            case 0 :
                return "Phone number"
            case 1 :
                return "E-mail address"
            case 2 :
                return "Address Info"
            default :
                return ""
            }
    }
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell!{
        var cell :UITableViewCell = self.userTable.dequeueReusableCellWithIdentifier("DetailCell2") as UITableViewCell
        
        if cell == UITableViewCell() {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "DetailCell2")
        }
        var cellText = ""
        var detailText = ""
        var x = ""
        switch (indexPath.section) {
        case 0 :
            switch indexPath.row{
            case 0 :
                cellText = contactInfo.objectForKey("mobileNumber") as NSString
                detailText = "Mobile Number"
            default:
                x = ""
                
            }
        case 1 :
            switch indexPath.row{
            case 0 :
                cellText = contactInfo.objectForKey("homeEmail") as NSString
                detailText = "Home Email"
            case 1 :
                cellText = contactInfo.objectForKey("workEmail") as NSString
                detailText = "Work Email"
            default :
                x = ""
                
                
            }
        case 2 :
            switch indexPath.row {
            case 0:
                cellText = contactInfo.objectForKey("address") as NSString
                detailText = "Street Adress"
            case 1:
                cellText = contactInfo.objectForKey("zip") as NSString
                detailText = "Zip Code"
            case 2:
                cellText = contactInfo.objectForKey("city") as NSString
                detailText = "City"
            default :
                x = ""
            }
        default:
            x = ""
        }
        
        
        cell.textLabel?.text = cellText
        //cell.detailTextLabel.text = detailText
        return cell
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func sendSMS(sender: UIBarButtonItem) {
        self.performPhoneAction(false)
    }
    
    @IBAction func pressCall(sender: AnyObject) {
        self.performPhoneAction(true)
    }

    func performPhoneAction(shouldMakeCall : Bool) {
        if contactInfo.objectForKey("mobileNumber") as NSString == "" && contactInfo.objectForKey("homeNumber") as NSString == "" {
            //var phoneOptions : UIActionSheet = UIActionSheet(title: "Pick a number", delegate: self, cancelButtonTitle: "close", destructiveButtonTitle: "", otherButtonTitles: contactInfo.objectForKey("homeNumber") as NSString , contactInfo.objectForKey("workNumber") as NSString ...)
            var smth = UIActionSheet(title: "Pick a number", delegate: self, cancelButtonTitle: "close", destructiveButtonTitle: "", otherButtonTitles: contactInfo.objectForKey("homeNumber") as NSString , contactInfo.objectForKey("mobileNumber") as NSString )
        
            smth.showInView(self.view)
            if shouldMakeCall {
                smth.tag = 101
            }
            else{
                smth.tag = 102
            }
            
            
        }
        else {
            var selectedPhoneNumber = ""
            if contactInfo.objectForKey("mobileNumber") as NSString != "" {
                selectedPhoneNumber = contactInfo.objectForKey("mobileNumber") as NSString
                
            }
            else {
                
            }
            if contactInfo.objectForKey("homeNumber") as NSString != "" {
                selectedPhoneNumber = contactInfo.objectForKey("homeNumber") as NSString
            }
            
            if selectedPhoneNumber != "" {
                if shouldMakeCall {
                    self.makeCallToNumber(selectedPhoneNumber)
                    
                }
                else {
                    self.sendSMSToNumber(selectedPhoneNumber)
                }
            }
        }
        
    }
    
    func makeCallToNumber(numberString : NSString) {
        var string = "tel:\(numberString)"
        var phoneURLNumber = NSURL(string: string)
        
        if UIApplication.sharedApplication().canOpenURL(phoneURLNumber){
            UIApplication.sharedApplication().openURL(phoneURLNumber)
        }
    }
    
    func sendSMSToNumber(numberToSend : NSString) {
        if !MFMessageComposeViewController.canSendText() {
            println("Can't send a message")
        }
        else {
            var sms = MFMessageComposeViewController()
            sms.messageComposeDelegate = self
            
            var arr = [numberToSend]
            sms.recipients = arr
            sms.body = "This is a message"
            self.presentViewController(sms, animated: true, completion: nil)
            
            
            
        }
    }
    func actionSheet(actionSheet: UIActionSheet!, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex != 3 {
            var selectedNumber = actionSheet.buttonTitleAtIndex(buttonIndex)
            if actionSheet.tag == 101 {
                self.makeCallToNumber(selectedNumber)
            }
            else {
                self.sendSMSToNumber(selectedNumber)
            }
    }
}
    func messageComposeViewController(controller: MFMessageComposeViewController!, didFinishWithResult result: MessageComposeResult) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
}
