//
//  myAgendaTableViewController.swift
//  ChatClien
//
//  Created by Tudor on 22/08/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit
import AddressBook
import AddressBookUI




class myAgendaTableViewController: UITableViewController ,UINavigationControllerDelegate,ABPeoplePickerNavigationControllerDelegate{
    
    @IBOutlet var EditButton: UIBarButtonItem!
    
    @IBOutlet var tabBar: UINavigationItem!
    @IBOutlet var table: UITableView!
    
    var empty = false
    var objects : NSMutableArray! = NSMutableArray()
    var addressBookController :ABPeoplePickerNavigationController!
    var contactsArray :NSMutableArray! =  NSMutableArray()
    var  ceva : NSString! = "asd"
    var image : UIImage!
    override func viewDidLoad() {
     //   self.tabBar.rightBarButtonItem.image = UIImage(named: "map-icon2")
//        var image = UIImage(named: "pin")
//        var typebutton = UIButtonType.Custom
//        var button = UIButton()
//        button.setImage(image, forState: UIControlState.Normal)
//        button.addTarget(self, action: "showAddressBook:", forControlEvents: UIControlEvents.TouchUpInside)
//        var custombutton :UIBarButtonItem = UIBarButtonItem(customView: button)
//        self.tabBar.rightBarButtonItem = custombutton
        
        super.viewDidLoad()
        
        //                self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    
    
    
    
    func peoplePickerNavigationController(peoplePicker: ABPeoplePickerNavigationController!, didSelectPerson person: ABRecordRef!) {
        
        var arrayItems = ["firstName","lastName","mobileNumber","homeNumber","homeEmail","workEmail","address","city","zip","image"]
        var valuesArray = ["","","","","","","","","",""]
        var dictionary  :NSMutableDictionary =  NSMutableDictionary(objects: valuesArray, forKeys: arrayItems   )
        
        var firstName: NSString! = Unmanaged<CFString>.fromOpaque(ABRecordCopyValue(person, kABPersonFirstNameProperty).toOpaque()).takeUnretainedValue().__conversion()
        var lastName = Unmanaged<CFString>.fromOpaque(ABRecordCopyValue(person, kABPersonLastNameProperty).toOpaque()).takeRetainedValue().__conversion()
        
        dictionary.setObject(firstName, forKey: "firstName")
        if lastName == "" {
            
        }
        else{
            dictionary.setObject(lastName, forKey: "lastName")
        }
        
        //        println("\(firstName) \(lastName)")
        //     Unmanaged<CFString>.fromOpaque(ABRecordCopyValue(person, kABPersonPhoneProperty).toOpaque()).takeRetainedValue().__conversion()
        
        let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
        let phones: ABMultiValueRef =
        Unmanaged.fromOpaque(unmanagedPhones.toOpaque()).takeUnretainedValue()
            as NSObject as ABMultiValueRef
        
        let countOfPhones = ABMultiValueGetCount(phones)
        var ct2 = 1
        for index in 0..<countOfPhones{
            let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, index)
            
            let phone: String = Unmanaged.fromOpaque(
                unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as String
            if ct2 == 1 {
                dictionary.setObject(phone, forKey: "mobileNumber")
                ct2 = ct2 + 1
            }
            else{
                dictionary.setObject(phone, forKey: "homeNumber")
                
            }
            
            // println(phone)
            
        }
        let unmanangedEmail = ABRecordCopyValue(person, kABPersonEmailProperty)
        let emails :ABMultiValueRef = Unmanaged.fromOpaque(unmanangedEmail.toOpaque()).takeUnretainedValue() as NSObject as ABMultiValueRef
        //var oneEmail = ABMultiValueCopyValueAtIndex(emails, 0)
        //var secEmail = ABMultiValueCopyValueAtIndex(emails, 1)
        //var email :String   = Unmanaged.fromOpaque(oneEmail.toOpaque()).takeUnretainedValue() as NSObject as String
        var workEmail :String = String()
        
        
        let countOfEmail = ABMultiValueGetCount(emails)
        var ct = 1
        for index2 in 0..<countOfEmail {
            var oneEmail = ABMultiValueCopyValueAtIndex(emails, index2)
            var email :String   = Unmanaged.fromOpaque(oneEmail.toOpaque()).takeUnretainedValue() as NSObject as String
            if ct==1 {
                dictionary.setObject(email ,forKey: "homeEmail")
                ct++
            }
            else {
                dictionary.setObject(email,forKey:"workEmail")
            }
            // print("\(email) is the email")
        }
        
        
        if ABPersonHasImageData(person) {
//            var unmanagedPhoto = ABRecordCopyValue(person, abpersoni)
  //          let photos = Unmanaged.fromOpaque(unmanagedPhoto.to )
             // var imageData  : CFDataRef = Unmanaged.fromOpaque(ABPersonCopyImageData(person).toOpaque()).takeUnretainedValue() as CFDataRef
              //self.image = UIImage(data: imageData)
            // var contactImage :CFData = ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail).takeRetainedValue()
              //dictionary.setObject(imageData, forKey: "image")
            
        }
        
        //            dictionary.setObject(workEmail,forKey:"workEmail")
        
        var unamangedAddressDictionary = ABRecordCopyValue(person, kABPersonAddressProperty)
        let addresses : ABMultiValueRef = Unmanaged.fromOpaque(unamangedAddressDictionary.toOpaque()).takeUnretainedValue() as NSObject as ABMultiValueRef
        //  var addressDictionary : NSDictionary = Unmanaged.fromOpaque(ABMultiValueCopyValueAtIndex(unamangedAddressDictionary, 0).toOpaque())
        var addressDictionary  = ABMultiValueCopyValueAtIndex(addresses, 0).takeUnretainedValue()  as NSDictionary
        if addressDictionary.objectForKey(kABPersonAddressCityKey) == nil {
            
        }
        else{
            var smth: AnyObject? = addressDictionary.objectForKey(kABPersonAddressCityKey)
           // dictionary.setObject(addressDictionary.objectForKey(kABPersonAddressCityKey) , forKey: "city")
//            dictionary.setObject(smth, forKey: "city")

            dictionary.setValue(addressDictionary.objectForKey(kABPersonAddressCityKey), forKey: "city")
        
        }
        if addressDictionary.objectForKey(kABPersonAddressZIPKey) == nil {
        }
        else {
        //    dictionary.setObject(addressDictionary.objectForKey(kABPersonAddressZIPKey), forKey: "zip")
           dictionary.setValue(addressDictionary.objectForKey(kABPersonAddressZIPKey), forKey: "zip")
        
        }
        if addressDictionary.objectForKey(kABPersonAddressStreetKey) == nil {
            
        }
        else{
           
        //    dictionary.setObject(addressDictionary.objectForKey(kABPersonAddressStreetKey), forKey: "address")
            dictionary.setValue(addressDictionary.objectForKey(kABPersonAddressStreetKey), forKey: "address")
        
        }
        
        contactsArray.addObject(dictionary)
        //  println(contactsArray)
        var fullName = firstName + " " + lastName
        self.empty =  true
        self.objects.addObject(fullName)
        self.tableView?.reloadData()
        self.addressBookController.dismissViewControllerAnimated(true, completion: nil)
        //println("\(dictionary)")
    }
    func peoplePickerNavigationController(peoplePicker: ABPeoplePickerNavigationController!, shouldContinueAfterSelectingPerson person: ABRecordRef!) -> Bool {
        return false;
        
        
    }
    
    func peoplePickerNavigationControllerDidCancel(peoplePicker: ABPeoplePickerNavigationController!) {
        self.addressBookController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AgendaCell") as myTableViewCell
        self.ceva = "mere"
        // Configure the cell...
        cell.customLabel.text = self.objects[indexPath.row] as NSString
        cell.customImage.image = UIImage(named: "profile-1")
        //cell.textLabel.text = self.objects[indexPath.row] as NSString
        return cell

    }
//    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
////        let cell = tableView.dequeueReusableCellWithIdentifier("AgendaCell", forIndexPath: indexPath) as UITableViewCell
//        let cell = tableView.dequeueReusableCellWithIdentifier("AgendaCell") as myTableViewCell
//        self.ceva = "mere"
//        // Configure the cell...
//        cell.customLabel.text = self.objects[indexPath.row] as NSString
//        cell.customImage.image = UIImage(named: "profile-1")
//        //cell.textLabel.text = self.objects[indexPath.row] as NSString
//        return cell
//    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.objects.count
    }
    
    
    //functia asta sa nu uiti s o comentezi
    override func tableView(tableView: UITableView ,didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let ContactDetailsView : NewViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageTo") as NewViewController
        
        let personDict = self.contactsArray.objectAtIndex(0) as NSMutableDictionary
        ContactDetailsView.contactInfo = personDict
        self.presentViewController(ContactDetailsView, animated: true, completion: nil)
        
    }
  
    
    
    
    @IBAction func pressedEdit(sender: UIBarButtonItem) {
       // self.objects.insertObject(NSDate.date(), atIndex: 0)
        var indexPath = NSIndexPath(forRow: 0, inSection: 0)
        var array :NSMutableArray = NSMutableArray()
        
        let ContactDetailsView : NewViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageTo") as NewViewController
        //    if contactsArray.count > 0 {
        
        if self.contactsArray.count > 0 {
        //let personDict = self.contactsArray.objectAtIndex(0) as NSMutableDictionary
        //println(personDict.objectForKey("firstName") )
        
            let ContactDetailsView : NewViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageTo") as NewViewController
            
            let personDict = self.contactsArray.objectAtIndex(0) as NSMutableDictionary
            ContactDetailsView.contactInfo = personDict
            self.presentViewController(ContactDetailsView, animated: true, completion: nil)        //  }
        //self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
        
    }
    @IBAction func showAddressBook(sender : UIBarButtonItem){
        addressBookController = ABPeoplePickerNavigationController()
        addressBookController.peoplePickerDelegate = self
        self.presentViewController(addressBookController, animated: true, completion: nil)
        
    }
}
