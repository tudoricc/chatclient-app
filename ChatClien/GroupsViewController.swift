//
//  GroupsViewController.swift
//  ChatClien
//
//  Created by Tudor on 11/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit

class GroupsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource,UITabBarDelegate , NSStreamDelegate {

    
    @IBOutlet var table: UITableView!
    @IBOutlet var button : UIButton!
    
    
    var name : NSString!
    var words = [" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "]
    var usersList = [" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "]
    
    //[[" "," "," "," "," "," "," "],[" "," "," "," "," "," "," "],[" "," "," "," "," "," "," "],[" "," "," "," "," "," "," "],[" "," "," "," "," "," "," "],[" "," "," "," "," "," "," "],[" "," "," "," "," "," "," "],[" "," "," "," "," "," "," "],[" "," "," "," "," "," "," "],[" "," "," "," "," "," "," "]]
    var selectedCells : NSIndexPath!
    var inputStream : NSInputStream!
    var outputStream : NSOutputStream!
    var pos = 0
    var posm = 0
    var firstTime = true
    var group :NSString!
    //var name : NSString!
    
    override func viewDidLoad() {
       
       // usersList.addObject("d")
        self.table.backgroundColor = UIColor.clearColor()
        self.table.opaque = false
        self.initNetworkCommunication()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bckgroups"))
//        var msg = "mygroups:\(self.name):sdadsa"
//        var res = outputStream.write(msg, maxLength: msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        //self.table?.reloadData()
//        self.button.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        self.button.sendActionsForControlEvents(UIControlEvents.TouchUpInside)

        var indexSet = NSIndexSet(index: 0)
        self.table.reloadSections(indexSet, withRowAnimation: UITableViewRowAnimation.Automatic)
        
        self.table.reloadData()
        
        self.button.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
         super.viewDidLoad()
        self.button.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
        self.table.reloadData()

        //self.button.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
      
        
        
               // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        println("dsads")
    }
    func tableView(tableView:UITableView ,numberOfRowsInSection section :NSInteger)->Int{
        return words.count
    }
    func tableView (tableView:UITableView , cellForRowAtIndexPath indexPath : NSIndexPath)->UITableViewCell{
        var cellIdentifier = "CellGroups"
        var cell :groupTableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as groupTableViewCell
        var item = words[indexPath.row] as NSString
        cell.groupName.text = item
        cell.alpha = 0.5
        if firstTime == false && selectedCells == indexPath{
            var users = usersList[indexPath.row]
            println("\(usersList[indexPath.row])")
            if cell.groupName.text == " " {
                println("empty")
            }
            else{
            cell.members.text = "\(users)"
            }
            
        }
        else {
        }
        return cell
    }
    func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return true
    }
    func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if (editingStyle == UITableViewCellEditingStyle.Delete){
            words.removeAtIndex(indexPath.row)
         //   tableView.delete
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedCells = indexPath
        self.firstTime = false
        self.group = words[indexPath.row]
        var msg = "groupsusers:\(self.name):\(words[indexPath.row]):sdadsa"
        var res = outputStream.write(msg, maxLength: msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        self.table.reloadData()
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if self.selectedCells != nil && indexPath == self.selectedCells {
            return 120
        }
        return 85
    }
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = table.cellForRowAtIndexPath(indexPath) as groupTableViewCell
        cell.iamge.hidden = true
      
        if indexPath == selectedCells {
           // usersList.removeAll(keepCapacity: true)
            table.reloadData()
        }
    }
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem!) {
        if item.title == "Go Back" {
            println("Go back")
            var UsersView    : ViewController2
            
            UsersView = self.storyboard?.instantiateViewControllerWithIdentifier("SecondView") as ViewController2
            UsersView.name = self.name
            self.inputStream.close()
            self.outputStream.close()
            
            self.viewWillDisappear(true)
            self.presentViewController(UsersView, animated: true, completion:nil)
            //self.presentViewController(UsersView, animated: true, completion:nil)
        }
        else if item.title == "Add to Group" {
            var view : insertToGroupViewController
            
            view = self.storyboard?.instantiateViewControllerWithIdentifier("AddGroup") as insertToGroupViewController
            view.name = self.name
            
            self.viewWillDisappear(true)
            self.presentViewController(view, animated: true, completion: nil)
            self.inputStream.close()
            self.outputStream.close()
        }
        else if item.title == "Create Group" {
            var view : createGroupController
            view = self.storyboard?.instantiateViewControllerWithIdentifier("CreateGroup") as createGroupController
            view.name = self.name
            self.viewWillDisappear(true)
            self.inputStream.close()
            self.outputStream.close()
            self.presentViewController(view, animated: true, completion: nil)
        }
        else  {
            //aici facem un r efresh
            var msg = "mygroups:\(self.name):sdadsa"
            var res = outputStream.write(msg, maxLength: msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
            
            
        }
    }
    
    
    
    func initNetworkCommunication() {
        
        
        //  declari niste stream-uri din care citesti si in care  scrii
        var readStream:Unmanaged<CFReadStream>?;
        var writeStream:Unmanaged<CFWriteStream>?     ;
        //localhost = 86.120.239.5
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,"localhost",8023,&readStream,&writeStream)
        
        inputStream = readStream!.takeUnretainedValue()
        outputStream = writeStream!.takeUnretainedValue()
        
        self.inputStream!.delegate=self
        self.outputStream!.delegate = self
        
        self.inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        
        //println("\(UsernameField.text)");
        var msg = "iam:\(self.name).groupView\r\n"
        
        //self.myname = msg.componentsSeparatedByString(":")[1] as NSString
        //println("\(msg)")
        var ptr = msg.nulTerminatedUTF8
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
        
    }

    func stream(theStream:NSStream , handleEvent streamEvent: NSStreamEvent) {
        switch(streamEvent){
        case NSStreamEvent.OpenCompleted :
            
            var ceva = ""
        case NSStreamEvent.HasBytesAvailable :
            if (theStream == inputStream){
                
                var len : Int
                var buffer : UnsafePointer<uint_fast8_t>!
                while (inputStream.hasBytesAvailable){
                    let buffer2=UnsafeMutablePointer<UInt8>.alloc(12230);
                    len = inputStream.read(buffer2, maxLength: 12230)
                    if(len>0){
                        var output :NSString = NSString(bytes: buffer2, length: len, encoding: NSASCIIStringEncoding)
                       
                        if ( output == ""){
                            
                        }
                        else{
                            if output.containsString("has joined"){
                                println("-------------->you have joined<-------------")
                            }
                            else if output.containsString("groups are"){
                                println("you have groups"   )
                                var groupsList = output.componentsSeparatedByString("\n")[1] as NSString
                                println("\(groupsList)")
                                var onlyTheGroups = groupsList.componentsSeparatedByString(":")
                                var size = onlyTheGroups.count
                                
                                for item in onlyTheGroups {
                               
                                   
                                        words[pos] = "\(item)"
                                        pos++
                                        self.table.reloadData()
                                    
                                }
                                pos = 0
                            }
                            else if output.containsString("message from the group"){
                                var group = output.componentsSeparatedByString("from the group:")[1] as NSString
                                var groupName = group.componentsSeparatedByString("and person")[0] as NSString
                                //var grupName = groupName.componentsSeparatedByString("from group:")[1] as NSString
                                var per = output.componentsSeparatedByString("and person:")[1] as NSString
                                var person = per.componentsSeparatedByString(" ")[0] as NSString
                                var msg2 = output.componentsSeparatedByString(" with the message\n")[1] as NSString
                                var ceva = msg2.componentsSeparatedByString("you have a message from the group")[0] as NSString
                                var banner :ALAlertBanner
                                banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "message from group: \(groupName)", subtitle: "\(person) said \(ceva)", tappedBlock: { (banner :ALAlertBanner!) in
                                    var ChatView : groupChatController
                                    //nu lasa asa pune sa afiseze ceview vri tu
                                    
                                    ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("groupChat") as groupChatController
                                    ChatView.group = groupName
                                    ChatView.name = self.name
                                    
                                    self.inputStream.close()
                                    self.outputStream.close()
                                    self.viewWillDisappear(true)
                                    self.presentViewController(ChatView, animated: true, completion:nil)
                                })
                                banner.exclusiveTouch = true
                                banner.show()
                            }

                            else if output.containsString("you have a message") {
                                println("YOU HAVE A MESSAGE")
                                var fromuser =  output.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[2] as NSString
                                var onlythemessage = output.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[6] as NSString
                                var banner :ALAlertBanner
                                banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "message from \(fromuser)", subtitle: onlythemessage, tappedBlock: { (banner :ALAlertBanner!) in
                                    var ChatView    : ViewController3
                                    //nu lasa asa pune sa afiseze ceview vri tu
                                    
                                    ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView") as ViewController3
                                    ChatView.nameofReceiver = fromuser
                                    ChatView.nameofSender = self.name
                                    
                                    self.inputStream.close()
                                    self.outputStream.close()
                                    self.viewWillDisappear(true)
                                    self.presentViewController(ChatView, animated: true, completion:nil)
                                })
                                banner.exclusiveTouch = true
                                banner.show()
                                
                                var localnotification:UILocalNotification = UILocalNotification()
                                localnotification.category = "FIRST_CATEGORY"
                                localnotification.alertBody = "\(fromuser) said: \(onlythemessage)"
                                localnotification.fireDate = NSDate(timeIntervalSinceNow: 5)
                                UIApplication.sharedApplication().scheduleLocalNotification(localnotification)

                                
                            }

                            else if output.containsString("users:"){
                                println("\(output) dasqwe qwe ")
                                var users = output.componentsSeparatedByString("users:")[1] as NSString
                               // var list = users.componentsSeparatedByString(":")
                                //println("\(list) ")
                              //  var x =
                                usersList[posm] = "\(users)"
                                
                                posm++
                                
                                self.table.reloadData()
                                //usersList.removeAll(keepCapacity: true)
                                //posm++
                            }
                            else if output.containsString("no online users"){
                                usersList[posm] = "No online users"
                                posm++
                            }
                                
                            else {
                                println("\(output) sad sda asd as ")
                            }
                            
                        }
                    }
                }
                
            }
        case NSStreamEvent.ErrorOccurred :
           var x = 1
        case NSStreamEvent.EndEncountered :
            
            var connectionAlert = UIAlertView(title: "Connection Error", message: "The server is down you can not do anything in this mode", delegate: self, cancelButtonTitle: "Close")
            self.view.addSubview(connectionAlert)
            connectionAlert.show()
        default:
            var ceva = ""
           
        }
    }
    
    @IBAction func getGroups(sender : UIButton) {
        pos = 0
        var msg = "mygroups:\(self.name):sdadsa"
        var res = outputStream.write(msg, maxLength: msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "groupChat" {
            println("How do oyu how ww")
            self.inputStream.close()
            self.outputStream.close()
            var dest = segue.destinationViewController as groupChatController
            dest.name = self.name
            dest.group = self.group
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
