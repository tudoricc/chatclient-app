//
//  JPThumbnailAnnotation.swift
//  ChatClien
//
//  Created by Tudor on 25/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit
import MapKit
protocol JPSThumbnailAnnotationProtocol{
    func annotationViewInMap(mapView : MKMapView)
}
class JPThumbnailAnnotation: NSObject , MKAnnotation , JPSThumbnailAnnotationProtocol {
    var coordinate :CLLocationCoordinate2D
    var view : JPThumbnailAnnotationView
    var thumbnail : JPThumbnail
    init(coord : CLLocationCoordinate2D,view :JPThumbnailAnnotationView,thumbnai:JPThumbnail ){
        self.coordinate = coord
        self.view = view
        self.thumbnail = thumbnai
        super.init()
    }
    init(thumbnail:JPThumbnail){
        coordinate = thumbnail.coordinate
        self.thumbnail = thumbnail
        self.view = JPThumbnailAnnotationView()
        super.init()
    }
    func annotationViewInMap(mapView: MKMapView) {
//        if self.view != JPThumbnailAnnotationView() {
            self.view = mapView.dequeueReusableAnnotationViewWithIdentifier("JPSThumbnailAnnotationView") as JPThumbnailAnnotationView
      //      if !self.view {
        var annot = self as JPThumbnailAnnotation
        self.view.annotation = self
        
    //        }
  //      }
        
    }
    
    
    func updateThumbnail(thumbnail:JPThumbnail , animated : Bool){
        if animated{
            UIView.animateWithDuration(NSTimeInterval(0.33), animations: {
                self.coordinate = thumbnail.coordinate
            })
        }
        else{
            self.coordinate = thumbnail.coordinate
        }
        self.view.updateWithThumbnail(thumbnail)
    }
    
    
}
