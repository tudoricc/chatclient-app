//
//  AddFriendViewController.swift
//  ChatClien
//
//  Created by Tudor on 04/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit

class AddFriendViewController: UIViewController,UITableViewDelegate,UITableViewDataSource ,UIAlertViewDelegate,UISearchBarDelegate, UISearchDisplayDelegate ,NSStreamDelegate{
    
    
    
    @IBOutlet var table: UITableView!
    var cells = ["mere" ,"pere","alune"]
    var results :[String] = []
    var candies = [Candy]()
    var filteredCandies = [Candy]()
    var nume :String!
    
    var inputStream :NSInputStream!
    var outputStream:NSOutputStream!
    
    
    override func viewDidLoad() {
        table.delegate = self
        table.dataSource = self
       

     
       
        self.initNetworkCommunication()
        // Reload the table
        self.nume = self.nume.componentsSeparatedByString(".")[0] as NSString
        var empty = [Candy]()
        var me = self.nume
        
        var mes = me.componentsSeparatedByString("/")[0] as NSString

        var msg = "possiblefriends:\(self.nume):dsads\n"
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        table.reloadData()
        NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "sendMessage2", userInfo: nil, repeats: false)
        table.reloadData()
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initNetworkCommunication() {
        
        
        //  declari niste stream-uri din care citesti si in care  scrii
        var readStream:Unmanaged<CFReadStream>?;
        var writeStream:Unmanaged<CFWriteStream>?     ;
        //localhost = 86.120.239.5
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,"localhost",8023,&readStream,&writeStream)
        
        inputStream = readStream!.takeUnretainedValue()
        outputStream = writeStream!.takeUnretainedValue()
        
        self.inputStream!.delegate=self
        self.outputStream!.delegate = self
        
        self.inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        //['possiblefriends', 'xcv', 'dsads\r\n']
        //['possiblefriends', 'ion', 'adszx\r\n']

        //println("\(UsernameField.text)");
        var msg = "iam:\(self.nume).friendView\r\n"
        
        self.nume = msg.componentsSeparatedByString(":")[1] as NSString
        //println("\(msg)")
        var ptr = msg.nulTerminatedUTF8
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
//        msg = "possiblefriends:\(self.nume):dsads\r\n"
//        res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
//        
        
    }
    func stream(theStream:NSStream , handleEvent streamEvent: NSStreamEvent) {
        switch(streamEvent){
        case NSStreamEvent.OpenCompleted :
           // println("Opened correctly")
            var ceva = ""
        case NSStreamEvent.HasBytesAvailable :
            if (theStream == inputStream){
                
                var len : Int
                var buffer : UnsafePointer<uint_fast8_t>!
                while (inputStream.hasBytesAvailable){
                    let buffer2=UnsafeMutablePointer<UInt8>.alloc(12230);
                    len = inputStream.read(buffer2, maxLength: 12230)
                    if(len>0){
                        var output :NSString = NSString(bytes: buffer2, length: len, encoding: NSASCIIStringEncoding)
                        //aici trebuie sa faci si tu un split sa vezi daca e numele tau cel de la care a trimis
                        //si daca e sa afisezi cu alta culoare si vezi daca nu e numele tau afisezi c u alta culoare
                        
                        //REMINDER: nu uita de treaba aia cu variabila dintr-un controller in altul
                        if ( output == ""){
                            
                        }
                        else{
                            // println("Server said : \(output)")
                            self.messageReceived(output)
                            
                            //println(" \(message)  ")
                            table?.reloadData()
                            
                        }
                    }
                }
                
            }
        case NSStreamEvent.ErrorOccurred :
           // println("Can not connect to the host")
            var connectionAlert = UIAlertView(title: "Connection Error", message: "The server is down you can not do anything in this mode", delegate: self, cancelButtonTitle: "Close")
            self.view.addSubview(connectionAlert)
            connectionAlert.show()
            
        case NSStreamEvent.EndEncountered :
            var connectionAlert = UIAlertView(title: "Connection Error", message: "The server is down you can not do anything in this mode", delegate: self, cancelButtonTitle: "Close")
            self.view.addSubview(connectionAlert)
            connectionAlert.show()
            
        default:
            var ceva = "sdasa"
           // println("Unknown event")
        }
    }
    
    func messageReceived(msg:NSString){
        var split :NSArray
        var text = msg
        
        split = msg.componentsSeparatedByString(":")
        var ceva = msg
        // text = split.objectAtIndex(1) as NSString
        // var ceva = String()
        
        if (msg.containsString("has joi")){
           //// println("you ave joined")
        }
        if (msg.containsString("ssible")){
           // println("dasd")
            var word = msg.componentsSeparatedByString("\n")[1] as NSString
            var people = word.componentsSeparatedByString(":")
            for i in 1..<people.count {
                var cuvant = "\(people[i])"
                var candy = Candy(category:"All", name: cuvant)
                self.candies.append(candy)
                
                
            }
        }
        if msg.containsString("are now friends"){
            var banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess , position: ALAlertBannerPositionTop, title: "Success",subtitle: "You are now friends" , tappedBlock:{ (banner :ALAlertBanner!) in
                var date = NSDate(timeIntervalSinceNow: 3)
                
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                banner.hide()
               
                
            })
            banner.show()
        
        
        }
        if msg.containsString("elationship not added") {
            //println("DSSADSA")
            var bannererror = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure, position: ALAlertBannerPositionTop, title: "Failure", subtitle: "Ÿour relationship has not been added", tappedBlock: {(banner: ALAlertBanner!) in
                var date  = NSDate(timeIntervalSinceNow: 3)
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                banner.hide()
            })
            bannererror.show()
        }
        if msg.containsString("don't have the second"){
           // println("DSSADS2A")
            var bannererror = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure, position: ALAlertBannerPositionTop, title: "Failure", subtitle: "Ÿour relationship has not been added", tappedBlock: {(banner: ALAlertBanner!) in
                var date  = NSDate(timeIntervalSinceNow: 3)
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                banner.hide()
            })
            bannererror.show()
        }
        if msg.containsString("don't have the first"){
            var bannererror = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleFailure, position: ALAlertBannerPositionTop, title: "Failure", subtitle: "Ÿour relationship has not been added", tappedBlock: {(banner: ALAlertBanner!) in
                var date  = NSDate(timeIntervalSinceNow: 3)
                var ceva2 = date.timeIntervalSinceDate(date)
                banner.secondsToShow = ceva2
                banner.hide()
            })
            bannererror.show()
        }
        if msg.containsString("message from the group"){
            var group = msg.componentsSeparatedByString("from the group:")[1] as NSString
            var groupName = group.componentsSeparatedByString("and person")[0] as NSString
            //var grupName = groupName.componentsSeparatedByString("from group:")[1] as NSString
            var per = msg.componentsSeparatedByString("and person:")[1] as NSString
            var person = per.componentsSeparatedByString(" ")[0] as NSString
            var msg2 = msg.componentsSeparatedByString(" with the message\n")[1] as NSString
            var ceva = msg2.componentsSeparatedByString("you have a message from the group")[0] as NSString
            var banner :ALAlertBanner
            banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "message from group: \(groupName)", subtitle: "\(person) said \(ceva)", tappedBlock: { (banner :ALAlertBanner!) in
                var ChatView : groupChatController
                //nu lasa asa pune sa afiseze ceview vri tu
                
                ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("groupChat") as groupChatController
                ChatView.group = groupName
                ChatView.name = self.nume
                
                self.inputStream.close()
                self.outputStream.close()
                self.viewWillDisappear(true)
                self.presentViewController(ChatView, animated: true, completion:nil)
            })
            banner.exclusiveTouch = true
            banner.show()
        }

        else if msg.containsString("you have a message") {
            println("YOU HAVE A MESSAGE")
            var fromuser =  msg.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[2] as NSString
            var onlythemessage = msg.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[6] as NSString
            var banner :ALAlertBanner
            banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "message from \(fromuser)", subtitle: onlythemessage, tappedBlock: { (banner :ALAlertBanner!) in
                var ChatView    : ViewController3
                //nu lasa asa pune sa afiseze ceview vri tu
                
                ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView") as ViewController3
                ChatView.nameofReceiver = fromuser
                ChatView.nameofSender = self.nume
                
                self.inputStream.close()
                self.outputStream.close()
                self.viewWillDisappear(true)
                self.presentViewController(ChatView, animated: true, completion:nil)
            })
            banner.exclusiveTouch = true
            banner.show()
            var localnotification:UILocalNotification = UILocalNotification()
            localnotification.category = "FIRST_CATEGORY"
            localnotification.alertBody = "\(fromuser) said: \(onlythemessage)"
            localnotification.fireDate = NSDate(timeIntervalSinceNow: 5)
            UIApplication.sharedApplication().scheduleLocalNotification(localnotification)

        }

        if msg.containsString("have that user"){
            println("There is no such user")
        }
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return self.filteredCandies.count
        } else {
            return self.candies.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //ask for a reusable cell from the tableview, the tableview will create a new one if it doesn't have any
        let cell = self.table.dequeueReusableCellWithIdentifier("addFriendCell") as UITableViewCell
        
        var candy :Candy
        // Check to see whether the normal table or search results table is being displayed and set the Candy object from the appropriate array
        if tableView == self.searchDisplayController!.searchResultsTableView {
            candy = filteredCandies[indexPath.row]
        } else {
            candy = candies[indexPath.row]
        }
        
        // Configure the cell
        cell.textLabel!.text = candy.name
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        var x = candies[indexPath.row]
         if candies[indexPath.row].name == ""   {
            println("em")
        }
        else {
            
            
            var him :NSString = candies[indexPath.row].name
           
            var me = self.nume
            var mes = me.componentsSeparatedByString("\r\n")[0] as NSString
            var msg = "addfriend:\(mes):\(him):dsads\r\n"
            var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
            

        }
    }
    
    func filterContentForSearchText(searchText: String) {
        
        self.filteredCandies = self.candies.filter({( candy: Candy) -> Bool in
            
            let stringMatch = candy.name.rangeOfString(searchText)
            return (stringMatch != nil)
        })
    }
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        self.filterContentForSearchText(searchString)
        return true
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text)
        return true
    }
    
 
    

    @IBOutlet var refresh: UIBarButtonItem!
    @IBAction func refreshAction(sender: AnyObject) {
        var msg = "possiblefriends:\(self.nume):dsads\r\n"
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        self.candies.removeAll(keepCapacity: true)
        
        
    }
    func sendMessage2(){
        self.candies.removeAll(keepCapacity: true)
        var msg = "possiblefriends:\(self.nume):dsads\r\n"
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        

    }
    
    @IBAction func goBack(sender: UIButton) {
        var FirstView :ViewController2
        FirstView = self.storyboard?.instantiateViewControllerWithIdentifier("SecondView") as ViewController2
        FirstView.name = self.nume
        self.presentViewController(FirstView, animated: true, completion:nil)
        self.viewWillDisappear(true)
    }
    func alertView(alertView: UIAlertView!, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            var firstView : ViewController
            
            
                firstView = self.storyboard?.instantiateViewControllerWithIdentifier("MainView") as ViewController
                self.presentViewController(firstView, animated: true, completion: nil)
                self.inputStream.close()
                self.outputStream.close()
                
        }
            self.storyboard?.instantiateInitialViewController()
        
    }
    
}
