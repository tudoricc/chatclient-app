//
//  MapViewController.swift
//  ChatClien
//
//  Created by Tudor on 14/08/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import MapKit

class MapViewController: UIViewController,MKMapViewDelegate, CLLocationManagerDelegate ,NSStreamDelegate{
    
    //@IBOutlet var SomeLabel: UILabel!
    
    @IBOutlet var harta: MKMapView!
    
    var region : MKCoordinateRegion!
    var locationManager :CLLocationManager! = CLLocationManager()
    var myname :NSString!
    var inputStream :NSInputStream!
    var outputStream : NSOutputStream!
    var mylatitude : CLLocationDegrees!
    var mylongitude : CLLocationDegrees!
    var beautifulname : NSString!
    var whoToSendMessageTo :NSString!
    var arrayOfPeople: NSMutableArray!
    var annotationArray : NSMutableArray = NSMutableArray()
    var name :NSString!
    var firstTime = true
    var only : UILabel!
    override func viewDidLoad() {
        
        var annotation :MKPointAnnotation = MKPointAnnotation()
        
        var myannotation :mySpecialAnnotation! = mySpecialAnnotation()
        
        var loc :CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 37.33233144, longitude: -122.0312188)
        var mere = "langa magazin"
        
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //println("\(self.harta.showsUserLocation)")
        //harta.userLocation.
        
        
        self.initNetworkCommunication()
        
        self.harta.delegate = self
        self.locationManager.delegate = self
        
        harta.showsUserLocation = true

        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.startUpdatingLocation()
        
        //asa adaugi o anotatie pe harta.gen saii spui unde este acel utilizator
        
        myannotation.initWithTitle(mere, Location: loc)
        annotation.coordinate = loc
        self.mylongitude = self.harta.userLocation.coordinate.longitude
        self.mylatitude = self.harta.userLocation.coordinate.latitude
        //println("\(self.mylongitude) \(self.mylatitude) blablabla ")
        
        
        //AICI O SA PUI myuserscoordinates:\(self.myname):dsadsa" ca l-am facut sa mearga bine
        var numeNice = self.myname.componentsSeparatedByString(".mapView\r\n")[0] as NSString
        println("\(numeNice) so niceee")
       outputStream.open()
        var msg2 = "myuserscoordinates:\(numeNice):ds"
        //I wrote it like this because the server immplements it withouth looking at what is after the ":"
        var res2 = outputStream.write(msg2, maxLength:msg2.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
        
        var infoLabel = UILabel()
       // CGRectMa
        var optionalLabel =  UILabel()
        
        optionalLabel.text = "shake the phone to go back"
        var width = optionalLabel.frame.width
        
        var button : UIButton = UIButton.buttonWithType(UIButtonType.InfoLight) as UIButton
//        button.buttonType = UIButtonType.InfoLight
        button.frame = CGRectMake(10, self.view.frame.height-50, 20  , 40)
        button.addTarget(self, action: "showInfo:", forControlEvents: UIControlEvents.TouchUpInside)
        button.addTarget(self, action: "hideInfo:", forControlEvents: UIControlEvents.TouchUpOutside)
        self.view.addSubview(button)
        infoLabel.frame = CGRectMake(0, 20, 250, 20)
        infoLabel.text = "In order to exit shake the phone"
        //self.view.addSubview(infoLabel)
        //sa decomentezi ce e mai jos in caz de ceva
        //self.harta.addAnnotation(myannotation)
        
        
        
        
      
    }
    
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if annotation.isKindOfClass(mySpecialAnnotation){
            var mylocation :mySpecialAnnotation
            mylocation =  annotation as mySpecialAnnotation
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("MyCustomAnnotation")
            if annotationView == nil {
                //annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "MyCustomAnnotation")
                annotationView = mylocation.annotationView()
//      //         UILabel(frame: CGRectMake(<#x: CGFloat#>, <#y: CGFloat#>, <#width: CGFloat#>, <#height: CGFloat#>))
                var smth = annotationView.annotation.title!.componentsSeparatedByString("(")[0] as NSString
                var anne = smth.componentsSeparatedByString("is")[0] as NSString
                var lbl = UILabel(frame: CGRectMake(annotationView.frame.origin.x , annotationView.frame.origin.y, CGFloat(anne.length*10) , 25))
             
                
                lbl.text = " \(anne)"
                lbl.font.fontWithSize(30)
                lbl.layer.borderColor = UIColor.grayColor().CGColor
                lbl.layer.borderWidth = 1.5
                lbl.layer.backgroundColor = UIColor.whiteColor().CGColor
                lbl.layer.cornerRadius = 15
                lbl.textColor = UIColor.blackColor()
                annotationView.addSubview(lbl)
                
                
              
                
                
                
                
                var rightButton :UIButton
                rightButton = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as UIButton
                rightButton.setTitle("Talk to him", forState: UIControlState.Normal)
                var title  = annotation.title
                //aici se face initialziarea
                //self.whoToSendMessageTo = annotation.title
                
                
                
                rightButton.addTarget(self, action: "pressButton:", forControlEvents: UIControlEvents.TouchUpInside)
                
                annotationView.rightCalloutAccessoryView = rightButton
                annotationView.enabled = true
               
                
                annotationView.canShowCallout = true
                //annotationView.frame = lbl.frame
            }
            else{
                annotationView.annotation = annotation
                            }
            return annotationView
        }
        else {
            return nil
        }
    }
    func mapView(mapView: MKMapView!, didAddAnnotationViews views: [AnyObject]!) {
        
    }
    func mapView(mapView: MKMapView!, didDeselectAnnotationView view: MKAnnotationView!) {
        for subview in view.subviews as [UIView] {
            if let labelView = subview as? UILabel {
                labelView.hidden = false
            }
        }
    }
    func mapView(mapView: MKMapView!, didSelectAnnotationView view: MKAnnotationView!) {
        
        //
              // println("\(view.annotation.title!) is the title")
    
        //
        
        var result = UILabel()
        for subview in view.subviews as [UIView]{
         
            if let labelView = subview as? UILabel {
                labelView.hidden = true
            }
            
        }
        
        
        var ceva = view.annotation.title!.componentsSeparatedByString(" ")[1] as NSString
        if ceva == "is" {
            self.whoToSendMessageTo = view.annotation.title!.componentsSeparatedByString(" ")[0] as NSString
        }
        else{
            self.whoToSendMessageTo = view.annotation.title!.componentsSeparatedByString(" ")[1] as NSString
        }
    }
    //centers the map on your location
    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!) {
        var loc :CLLocationCoordinate2D  = userLocation.coordinate
        var x = loc.latitude
        var y = loc.longitude
        self.mylatitude = x
        self.mylongitude = y
        // println("My location is \(x) and \(y)")
        if self.firstTime == true {
            var region  = MKCoordinateRegionMakeWithDistance(loc, 500, 500)
            self.harta.region = region
            firstTime = false
        }
            
        //trimit la server locatia mea
        var sendingName: AnyObject = self.myname.componentsSeparatedByString(".")[0] as NSString
        var msg = "setcoordinates:\(sendingName):\(x):\(y)"
        var res = outputStream.write(msg, maxLength: msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        self.beautifulname = sendingName as NSString
        
        
    }
    
    
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        var degrees = newLocation.coordinate.latitude
        var decimal = fabs(newLocation.coordinate.latitude - degrees)
        var minutes =  decimal * 60
        var seconds = decimal * 3600 - minutes * 60
        //this is your curent location you can use this to send to the server
        var lat = "\(degrees),\(minutes),\(seconds)"
        //println(lat )
        degrees = newLocation.coordinate.longitude
        decimal = fabs(newLocation.coordinate.longitude - degrees)
        minutes  = decimal * 60
        seconds = decimal * 3600 - minutes * 60
        
        var longitude = "\(degrees),\(minutes),\(seconds)"
        //println(longitude)
    }
    
    
    
    func initNetworkCommunication() {
        
        
        //  declari niste stream-uri din care citesti si in care  scrii
        var readStream:Unmanaged<CFReadStream>?;
        var writeStream:Unmanaged<CFWriteStream>?     ;
        //localhost = 86.120.239.5
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,"localhost",8023,&readStream,&writeStream)
        
        inputStream = readStream!.takeUnretainedValue()
        outputStream = writeStream!.takeUnretainedValue()
        
        self.inputStream!.delegate=self
        self.outputStream!.delegate = self
        
        self.inputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        self.outputStream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        self.inputStream.open()
        self.outputStream.open()
        
        //println("\(UsernameField.text)");
        var msg = "iam:\(self.myname).mapView\r\n"
    
        self.myname = msg.componentsSeparatedByString(":")[1] as NSString
        //println("\(msg)")
        self.name = self.myname.componentsSeparatedByString(".")[0] as NSString
        var ptr = msg.nulTerminatedUTF8
        var res = outputStream.write(msg, maxLength:msg.lengthOfBytesUsingEncoding(NSASCIIStringEncoding))
        
        
    }
    func stream(theStream:NSStream , handleEvent streamEvent: NSStreamEvent) {
        switch(streamEvent){
        case NSStreamEvent.OpenCompleted :
            println("Opened correctly")
        case NSStreamEvent.HasBytesAvailable :
            if (theStream == inputStream){
                
                var len : Int
                var buffer : UnsafePointer<uint_fast8_t>!
                while (inputStream.hasBytesAvailable){
                    let buffer2=UnsafeMutablePointer<UInt8>.alloc(1230);
                    len = inputStream.read(buffer2, maxLength: 1230)
                    if(len>0){
                        var output = NSString(bytes: buffer2, length: len, encoding: NSASCIIStringEncoding)
                        
                        if ( output == ""){
                            
                        }
                        else{
                            // println("Server said : \(output)")
                            self.messageReceived(output)
                            
                        }
                    }
                }
                
            }
        case NSStreamEvent.ErrorOccurred :
            println("Can not connect to the host")
        case NSStreamEvent.EndEncountered :
            println("Here it is")
        case NSStreamEvent.HasSpaceAvailable :
            //you really don't have to print anything here because this case is when te stream has byes available,nothing else
            //println("It has space available")
            print("")
        default:
            println("Unknown event")
        }
    }
    func messageReceived(msg:NSString){
        var split :NSArray
        var text = msg
        var index = 0
        
        split = msg.componentsSeparatedByString(":")
        var ceva = msg
        
        if msg.containsString("has joined"){
            //you have just logged in ;Nothing to add at the moment
            println("You have joined")
        }
        else if msg.containsString("you have a message") {
            println("YOU HAVE A MESSAGE")
            var fromuser =  msg.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[2] as NSString
            var onlythemessage = msg.componentsSeparatedByString("\n")[1].componentsSeparatedByString(":")[6] as NSString
            var banner :ALAlertBanner
            banner = ALAlertBanner(forView: self.view, style: ALAlertBannerStyleSuccess, position: ALAlertBannerPositionTop, title: "message from \(fromuser)", subtitle: onlythemessage, tappedBlock: { (banner :ALAlertBanner!) in
                var ChatView    : ViewController3
                //nu lasa asa pune sa afiseze ceview vri tu
                
                ChatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView") as ViewController3
                ChatView.nameofReceiver = fromuser
                ChatView.nameofSender = self.name
                
                self.viewWillDisappear(true)
                self.presentViewController(ChatView, animated: true, completion:nil)
            })
            banner.exclusiveTouch = true
            banner.show()
            var localnotification:UILocalNotification = UILocalNotification()
            localnotification.category = "FIRST_CATEGORY"
            localnotification.alertBody = "\(fromuser) said: \(onlythemessage)"
            localnotification.fireDate = NSDate(timeIntervalSinceNow: 5)
            UIApplication.sharedApplication().scheduleLocalNotification(localnotification)

        }
        
        if msg.containsString("you just added your coordinates") {
            //you added your coordinates;Nothing yet to do here
            println("You have added coordinates")
        }
        if msg.containsString("Coordinates"){
            println("You are receiving coordinates")
            println("the message is \(msg)")
            var payload  = msg.componentsSeparatedByString("Coordinates \n")[1] as NSString
            var vect = payload.componentsSeparatedByString("\n")
            var x = 0
            var string : String
            //            var everyone :String
//            
//            if vect.count == 1  {
//                
//            }
//            else {
                print("\(payload) este dimensiunea")
                x = 0
                while index < vect.count {
                    var line = vect[index] as NSString
                    //var name = payload.componentsSeparatedByString("\n")[x] as NSString
                    //var ceva = line.componentsSeparatedByString(" ")[x+1] as NSString
                    var name2 = line.componentsSeparatedByString(":")[0] as NSString
                    var lat = line.componentsSeparatedByString(":")[1]  as NSString
                    var long = line.componentsSeparatedByString(":")[2] as NSString
                    
                    var numberFormatter = NSNumberFormatter()
                    
                    numberFormatter.maximumFractionDigits = 8
                    var nsstringLatitude = lat
                    var latitude = nsstringLatitude.floatValue
                    var formattedLatitude = numberFormatter.stringFromNumber(latitude)
                    
                    var nsstringLongitude = long
                    var longitude = nsstringLongitude.floatValue
                    var formatteLongitude = numberFormatter.stringFromNumber(longitude)
                    
                    println ("\(name2):TheName \(latitude):TheLatitude \(longitude):TheLongitude")
                    //self.whoToSendMessageTo = name2
                  //  arrayOfPeople.addObject(name2)
                    
                    var annotation :MKPointAnnotation = MKPointAnnotation()
                    var locationOfAnnotation :CLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(latitude), CLLocationDegrees(longitude))
                    
                    
                    // self.harta.addAnnotation(annotation)
                    
                    var madalinaAnnotation :mySpecialAnnotation = mySpecialAnnotation()
                    madalinaAnnotation.initWithTitle("\(name2) is here", Location: locationOfAnnotation)
                    annotationArray.addObject(madalinaAnnotation)
                    //nnotationView.canShowCallout = true
                    //annotationView.frame = label.frame
                    var label :UILabel = UILabel(frame: CGRectMake(madalinaAnnotation.annotationView().frame.origin.x, madalinaAnnotation.annotationView().frame.origin.y, 100, 50))
                    
                    label.text = "CE MAI FACI TUDORICA FARA FRICA"
                    label.textColor = UIColor.redColor()
                    madalinaAnnotation.annotationView().addSubview(label)
                     //madalinaAnnotation.annotationView().frame = label.frame
                    madalinaAnnotation.annotationView().enabled = true
                    
                   // self.harta.selectAnnotation(madalinaAnnotation, animated: true)
                    //                    self.harta.selectAnnotation(madalinaAnnotation, animated: true)
                    //self.harta.addAnnotation(madalinaAnnotation)
                    
                    x=0
                    index++
                }
                index = 0
                
                self.harta.addAnnotations(annotationArray)
            
                //self.harta.selectAnnotation(annotationArray, animated: true)
                            //}
        }
        
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.becomeFirstResponder()
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent) {
        if(event.subtype == UIEventSubtype.MotionShake) {
            //println("Stop shaking it bro")
            var secondView : ViewController2
            secondView = self.storyboard?.instantiateViewControllerWithIdentifier("SecondView") as ViewController2
            
            secondView.name = self.myname.componentsSeparatedByString(".")[0] as NSString
            
            //Update:19.08.14 :You don't have to close this connection so we don't lose the coordinates of the client
            //            self.inputStream.close()
            //            self.outputStream.close()
            self.presentViewController(secondView, animated: true, completion: nil)
            //self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    @IBAction func pressButton(sender : UIButton){
        println("You can go to another chat" )
        var chatView :ViewController3
        chatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView") as ViewController3
        chatView.nameofReceiver = self.whoToSendMessageTo
        chatView.nameofSender = self.name
        self.presentViewController(chatView, animated: true, completion: nil)
        self.viewWillDisappear(true)
        
        
    }
    @IBAction func showInfo(sender:UIButton) {
//        button.frame = CGRectMake(10, self.view.frame.height-50, 20  , 40)
        var word = "shake to exit"
        var x : CGFloat = 10
       
        var label = UILabel()
        label.text = word
        label.font.fontWithSize(10)
        label.alpha = 0.7
        label.clipsToBounds = true
        label.layer.cornerRadius = 5
        
        
        label.backgroundColor = UIColor.whiteColor()
        label.frame = CGRectMake( 10 , self.view.frame.height-60, 100 , 20  )
        
       // NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: "changeImage", userInfo: nil, repeats: true)
        self.view.addSubview(label)
        
        
    }
    @IBAction func hideInfo(sender:UIButton){
        self.only.hidden = true
    }
    
}
