//
//  JPThumbnail.swift
//  ChatClien
//
//  Created by Tudor on 25/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit
import MapKit
class JPThumbnail: NSObject {
    var image : UIImage = UIImage()
    var title : NSString = ""
    var subtitle : NSString = ""
    var coordinate : CLLocationCoordinate2D
    typealias SequencerNext = (Void -> Void)
    
    init(image2:UIImage , title2:NSString , subtitle2 :NSString , coordinate2:CLLocationCoordinate2D){
        self.image = image2
        self.title = title2
        self.subtitle = subtitle2
        self.coordinate = coordinate2
    }
}
