//
//  recoverPasswordController.swift
//  ChatClien
//
//  Created by Tudor on 26/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit
import MessageUI

class recoverPasswordController: UIViewController,MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func requestPassword(sender : UIButton) {
        var controller = MFMailComposeViewController()
        controller.mailComposeDelegate = self
        controller.setSubject("Recover password")
        controller.setMessageBody("Heelo there", isHTML: false)
        if controller != MFMailComposeViewController() {
            self.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
//        if result == MFMailComposeResultSent {
//            println("you sent it")
//        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
