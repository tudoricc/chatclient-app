//
//  JPThumbnailAnnotationView.swift
//  ChatClien
//
//  Created by Tudor on 25/09/14.
//  Copyright (c) 2014 meforme. All rights reserved.
//

import UIKit
import MapKit


protocol JPThumbnailAnnotationViewProtocol {
    // protocol definition goes here
    func didSelectAnnotationViewInMap(mapView : MKMapView)
    func didDeselectAnnotationViewInMap(mapView : MKMapView)
    
}
class JPThumbnailAnnotationView: MKAnnotationView,JPThumbnailAnnotationViewProtocol {



enum  JPThumbnailAnnotationViewAnimationDirection : Int {
    case JPThumbnailAnnotationViewAnimationDirectionGrow
    case JPThumbnailAnnotationViewAnimationDirectionShrink
}
enum JPThumbnailAnnotationViewState : Int {
    case JPThumbnailAnnotationViewStateCollapsed
    case JPThumbnailAnnotationViewStateExpanded
    case JPThumbnailAnnotationViewStateAnimating
    
}


   
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */
    let kJPSThumbnailAnnotationViewReuseID = "JPSThumbnailAnnotationView"
    let kJPSThumbnailAnnotationViewStandardWidth     = CGFloat(75.0)
    let kJPSThumbnailAnnotationViewStandardHeight    = CGFloat(87.0)
    let kJPSThumbnailAnnotationViewExpandOffset      = CGFloat(200.0)
    let kJPSThumbnailAnnotationViewVerticalOffset    = CGFloat(34.0)
    let kJPSThumbnailAnnotationViewAnimationDuration = CGFloat(0.25)
    
    var coordinate :CLLocationCoordinate2D
    var imageView : UIImageView
    var titleLabel : UILabel
    var subtitleLabel : UILabel
   // var disclosureBlock : JPThumbnail.SequencerNext
    var bgLayer :CAShapeLayer
    var disclosureButton : UIButton
    var state : JPThumbnailAnnotationViewState
    override init(){
        self.coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
        self.imageView = UIImageView()
        self.imageView = UIImageView()
        self.state = JPThumbnailAnnotationViewState.JPThumbnailAnnotationViewStateAnimating
        self.titleLabel = UILabel()
        //self.disclosureBlock = JPThumbnail.SequencerNext("ds")
        self.bgLayer = CAShapeLayer()
        self.disclosureButton = UIButton()
        self.subtitleLabel = UILabel()
        super.init()

    }
    init(coordinate :CLLocationCoordinate2D , imageView : UIImageView,titleLabel : UILabel , subtitleLabel : UILabel , disclosureBlock : JPThumbnail.SequencerNext ,bgLayer :CAShapeLayer ,disclosureButton : UIButton , state : JPThumbnailAnnotationViewState){
        
        self.coordinate = coordinate
        self.imageView = imageView
        self.state = state
        self.titleLabel = titleLabel
    //    self.disclosureBlock = disclosureBlock
        self.bgLayer = bgLayer
        self.disclosureButton = disclosureButton
        self.subtitleLabel = subtitleLabel
        super.init()
        
    }
    init(annotation : MKAnnotation) {
        self.coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
        self.imageView = UIImageView()
        self.state = JPThumbnailAnnotationViewState.JPThumbnailAnnotationViewStateAnimating
        self.titleLabel = UILabel()
        //self.disclosureBlock = JPThumbnail.SequencerNext("ds")
        self.bgLayer = CAShapeLayer()
        self.disclosureButton = UIButton()
        self.subtitleLabel = UILabel()
        super.init(annotation: annotation, reuseIdentifier: kJPSThumbnailAnnotationViewReuseID)
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initWithAnnotation(annotation : MKAnnotation) ->JPThumbnailAnnotationView {
    
        self.initWithAnnotation(annotation)
        var annot = MKAnnotationView(annotation: annotation, reuseIdentifier: kJPSThumbnailAnnotationViewReuseID) as JPThumbnailAnnotationView
        
        self.canShowCallout = true
            self.frame = CGRectMake(0, 0, kJPSThumbnailAnnotationViewStandardWidth, kJPSThumbnailAnnotationViewStandardHeight)
            self.centerOffset = CGPointMake(0, -kJPSThumbnailAnnotationViewVerticalOffset)
        self.state = JPThumbnailAnnotationViewState.JPThumbnailAnnotationViewStateCollapsed
        self.setUpView()
        return self
        
    }
    func disclosureButtonImage() -> UIImage{
        var size = CGSizeMake(21, 36)
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.mainScreen().scale)
        var berzierPath = UIBezierPath()
        berzierPath.moveToPoint(CGPointMake(2, 2))
        berzierPath.addLineToPoint(CGPointMake(10, 10))
        berzierPath.addLineToPoint(CGPointMake(2, 18))
        UIColor.lightGrayColor().setStroke()
        berzierPath.lineWidth = 3
        berzierPath.stroke()
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    func setUpView(){
        self.setupImageView()
        self.setUpTitleLabel()
        self.setUpSubtitleLabel()
        self.setUpDisclosureButton()
        self.setLayerProperties()
        self.setDetailGroupAlpha(0)
       
    }
    func setupImageView(){
        self.imageView = UIImageView(frame: CGRectMake(12.5, 12.5, 50, 47))
        self.imageView.layer.cornerRadius = 4
        self.imageView.layer.masksToBounds = true
        self.imageView.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.imageView.layer.borderWidth = 0.5
        self.addSubview(self.imageView)
    }
    
    func setUpTitleLabel(){
        titleLabel = UILabel(frame: CGRectMake(-32, 16, 168, 20))
        titleLabel.textColor = UIColor.darkTextColor()
        titleLabel.font = UIFont.boldSystemFontOfSize(17)
        titleLabel.minimumScaleFactor = 0.8
        titleLabel.adjustsFontSizeToFitWidth = true
        self.addSubview(titleLabel)
    }
    
    func setUpSubtitleLabel(){
        subtitleLabel = UILabel(frame: CGRectMake(-32, 36, 168, 20))
        subtitleLabel.textColor = UIColor.grayColor()
        subtitleLabel.font = UIFont.systemFontOfSize(12)
        self.addSubview(titleLabel)
    }
    func setUpDisclosureButton(){
        var device = UIDevice.currentDevice().systemVersion as NSString
        var deviceVersion = device.floatValue
        var type : UIButtonType
        
        if deviceVersion >= 7 {
            type = UIButtonType.System
        }
        else {
            type = UIButtonType.Custom
        }
        disclosureButton = UIButton.buttonWithType(type) as UIButton
        disclosureButton.tintColor = UIColor.grayColor()
        var disclosureIndImage : UIImage = JPThumbnailAnnotationView.disclosureButtonImage(self)()
        disclosureButton.setImage(disclosureIndImage, forState: UIControlState.Normal)
        disclosureButton.frame = CGRectMake(kJPSThumbnailAnnotationViewExpandOffset/2.0 + self.frame.size.width/2.0 + 8.0,26.5,disclosureIndImage.size.width,   disclosureIndImage.size.height)
        disclosureButton.addTarget(self, action: "didTap", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(disclosureButton)
        
    }
    func setLayerProperties(){
        bgLayer = CAShapeLayer()
        var pathref = self.newBubbleWithRect(self.bounds) as CGPathRef
        bgLayer.path = pathref
        bgLayer.fillColor = UIColor.whiteColor().CGColor
        bgLayer.shadowColor = UIColor.blackColor().CGColor
        bgLayer.shadowOffset = CGSizeMake(0, 3)
        bgLayer.shadowRadius = 2
        bgLayer.shadowOpacity = 0.5
        bgLayer.masksToBounds = false
        self.layer.insertSublayer(bgLayer, atIndex: 0)
    }
    
    func setDetailGroupAlpha(alpha:CGFloat){
        self.disclosureButton.alpha = alpha
        self.titleLabel.alpha = alpha
        self.subtitleLabel.alpha = alpha
    }
    
    func newBubbleWithRect(rect : CGRect) -> CGPathRef{
        var stroke = 1.0
        var radius = 7.0
        var path = CGPathCreateMutable()
        var parentX = rect.origin.x + rect.size.width/2
        
       var rect2 = CGRectMake(rect.origin.x + CGFloat(stroke)/2 + 7, rect.origin.y + CGFloat(stroke)/2 + 7, rect.size.width - CGFloat(stroke) + 14.0
, rect.size.height - CGFloat(stroke) + 29)
        CGPathMoveToPoint(path, nil, rect.origin.x, rect.origin.y + CGFloat(radius));
        CGPathAddLineToPoint(path, nil, rect.origin.x, rect.origin.y + rect.size.height - CGFloat(radius));
        CGPathAddArc(path, nil, rect.origin.x + CGFloat(radius), rect.origin.y + rect.size.height - CGFloat(radius), CGFloat(radius), CGFloat(M_PI),CGFloat(M_PI_2), 1);
        CGPathAddLineToPoint(path, nil, parentX - 14.0, rect.origin.y + rect.size.height);
        CGPathAddLineToPoint(path, nil, parentX, rect.origin.y + rect.size.height + 14.0);
        CGPathAddLineToPoint(path, nil, parentX + 14.0, rect.origin.y + rect.size.height);
        CGPathAddLineToPoint(path, nil, rect.origin.x + rect.size.width - CGFloat(radius) , rect.origin.y + rect.size.height);
        CGPathAddArc(path, nil, rect.origin.x + rect.size.width - CGFloat(radius), rect.origin.y + rect.size.height - CGFloat(radius), CGFloat(radius), CGFloat(M_PI_2), 0.0, 1.0);
        CGPathAddLineToPoint(path, nil, rect.origin.x + rect.size.width, rect.origin.y + CGFloat(radius));
        CGPathAddArc(path, nil, rect.origin.x + rect.size.width - CGFloat(radius), rect.origin.y + CGFloat(radius), CGFloat(radius), 0.0, -CGFloat(M_PI_2), 1.0);
        CGPathAddLineToPoint(path, nil, rect.origin.x + CGFloat(radius), rect.origin.y);
        CGPathAddArc(path, nil, rect.origin.x + CGFloat(radius), rect.origin.y + CGFloat(radius), CGFloat(radius), -CGFloat(M_PI_2), CGFloat(M_PI), 1.0);
        CGPathCloseSubpath(path);
        return path;

        
    }
    func didSelectAnnotationViewInMap(mapView : MKMapView){
        mapView.setCenterCoordinate(self.coordinate, animated: true)
        self.expand()
    
    }
    func expand(){
        if self.state != JPThumbnailAnnotationViewState.JPThumbnailAnnotationViewStateCollapsed {
         return
        }
        self.state = JPThumbnailAnnotationViewState.JPThumbnailAnnotationViewStateAnimating
        
        self.animateBubbleWithDirection(JPThumbnailAnnotationViewAnimationDirection.JPThumbnailAnnotationViewAnimationDirectionGrow)
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width + kJPSThumbnailAnnotationViewExpandOffset, self.frame.size.height)
        self.centerOffset = CGPointMake(kJPSThumbnailAnnotationViewExpandOffset/2, -kJPSThumbnailAnnotationViewVerticalOffset)
        //UIView.animateWithDuration(<#duration: NSTimeInterval#>, delay: <#NSTimeInterval#>, options: <#UIViewAnimationOptions#>, animations: <#() -> Void##() -> Void#>, completion: <#((Bool) -> Void)?##(Bool) -> Void#>)
        UIView.animateWithDuration(NSTimeInterval(kJPSThumbnailAnnotationViewAnimationDuration / 2), delay: NSTimeInterval(kJPSThumbnailAnnotationViewAnimationDuration), options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.setDetailGroupAlpha(1)
            }, completion: {(finished : Bool) in
                self.state = JPThumbnailAnnotationViewState.JPThumbnailAnnotationViewStateExpanded
        })
        
    }
    func didDeselectAnnotationViewInMap(mapView : MKMapView){
        self.shrink()
    }
    func shrink(){
        if self.state != JPThumbnailAnnotationViewState.JPThumbnailAnnotationViewStateExpanded{
            return
        }
        self.state = JPThumbnailAnnotationViewState.JPThumbnailAnnotationViewStateAnimating
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width - kJPSThumbnailAnnotationViewExpandOffset, self.frame.size.height)
        self.centerOffset = CGPointMake(kJPSThumbnailAnnotationViewExpandOffset/2, -kJPSThumbnailAnnotationViewVerticalOffset)
        //UIView.animateWithDuration(<#duration: NSTimeInterval#>, delay: <#NSTimeInterval#>, options: <#UIViewAnimationOptions#>, animations: <#() -> Void##() -> Void#>, completion: <#((Bool) -> Void)?##(Bool) -> Void#>)
        UIView.animateWithDuration(NSTimeInterval(kJPSThumbnailAnnotationViewAnimationDuration / 2), delay: NSTimeInterval(0), options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.setDetailGroupAlpha(0)
            }, completion: {(finished : Bool) in
                self.animateBubbleWithDirection(JPThumbnailAnnotationViewAnimationDirection.JPThumbnailAnnotationViewAnimationDirectionShrink)
                self.state = JPThumbnailAnnotationViewState.JPThumbnailAnnotationViewStateExpanded
        })

    }
    
    
    func animateBubbleWithDirection(animationDirection : JPThumbnailAnnotationViewAnimationDirection){
        var growing = (animationDirection == JPThumbnailAnnotationViewAnimationDirection.JPThumbnailAnnotationViewAnimationDirectionGrow)
//        UIView.animateWithDuration(kJPSThumbnailAnnotationViewAnimationDuration, animations: {
//            var xoffset = (growing ? -1 : 1) * kJPSThumbnailAnnotationViewExpandOffset/2
//            self.imageView.frame = CGRectOffset(self.imageView.frame, xoffset, 0)
//        })
        var delay : NSTimeInterval = NSTimeInterval(kJPSThumbnailAnnotationViewAnimationDuration)
        UIView.animateWithDuration(delay, animations: { () -> Void in
            var xoffset = (growing ? -1 : 1) * self.kJPSThumbnailAnnotationViewExpandOffset/2
            self.imageView.frame = CGRectOffset(self.imageView.frame, xoffset, 0)

            }, completion: {(finished : Bool) in
                if animationDirection == JPThumbnailAnnotationViewAnimationDirection.JPThumbnailAnnotationViewAnimationDirectionShrink{
                    self.state = JPThumbnailAnnotationViewState.JPThumbnailAnnotationViewStateCollapsed
                }
            } )
        
        
        var animation :CABasicAnimation = CABasicAnimation(keyPath: "path")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.repeatCount = 1
        animation.removedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        animation.duration = CFTimeInterval(kJPSThumbnailAnnotationViewAnimationDuration)
        
        var largeRect = CGRectInset(self.bounds, -kJPSThumbnailAnnotationViewExpandOffset/2, 0)
        var fromPath = self.newBubbleWithRect(growing ? self.bounds : largeRect)
        animation.fromValue = fromPath
        
        var toPath = self.newBubbleWithRect(growing ? largeRect : self.bounds)
        animation.toValue = toPath
        self.bgLayer.addAnimation(animation, forKey: animation.keyPath)
        
        
    }
    func updateWithThumbnail(var thumbnail:JPThumbnail){
        self.coordinate = thumbnail.coordinate
        self.titleLabel.text = thumbnail.title
        self.subtitleLabel.text = thumbnail.subtitle
        self.imageView.image = thumbnail.image
    
    }
    
    func didTapDisclosureButton() {
       // if (self.disclosureBlock != nil) {
      //      self.disclosureBlock()
        //}
    }
    
}
