# First Commit : 14.08.2014 #

## **What we did so far:** ##

### Implemented the server with some basic functions. ###

### Functions like ###
  
     -connecting to the server[if there is another one like you it doesn't allow you to connect] 
       
     -broadcasting a message to everybody [msg:bodyofmessage] 

     -sending a message to a specific person[checks if it exists or not]
  
     -creating a group
  
     -sending a message to a group


For now everything is lasting as long as the server is on.we don't have a database yet so we can keep track of messages ,users logging in , groups .This will we worked on in the future.Right now we have a beta thing,really really beta.

### Developed an iPhone app which allows you to: ###

     -log in

     -see everybody who is online

     -send a message to another user (from A to B)

     -receive a message from another user (from B to A) 

     -receive a message from another user while talking to another one(A is talking to B ,then C says something to A so he can choose whether to talk to him or not.clicks a notification]

     -front end still kind of sucks because "beta" and because "tudor"


# Second major commit : 28.08.2014 #

## What we did after the first commit:##

-implemented the map mode
-implemented offline mode
-partially implemented the registration form[still on the server]

## Details: ##
	Map Mode:
	
	- show your location
	- show everybody else's location
	- open a new view when you click on a person's name[annotation]
	- receive a notification[nice one] when you get a message and you can go chat with that person
	- shake motion to go back

	
	Offline Mode :
	
	- get all the contacts from your agenda
	-show information for each contact
	-call a contact
	-message a contacts
	
	Registration form :
	
	-send a request to the server to get authenticated




**For further information:**

The Asana page of this project is :
https://app.asana.com/0/15106239257862/15106239257862/15106239257862/15106239257862