
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AKTabBarController
#define COCOAPODS_POD_AVAILABLE_AKTabBarController
#define COCOAPODS_VERSION_MAJOR_AKTabBarController 1
#define COCOAPODS_VERSION_MINOR_AKTabBarController 1
#define COCOAPODS_VERSION_PATCH_AKTabBarController 0

// ALAlertBanner
#define COCOAPODS_POD_AVAILABLE_ALAlertBanner
#define COCOAPODS_VERSION_MAJOR_ALAlertBanner 0
#define COCOAPODS_VERSION_MINOR_ALAlertBanner 3
#define COCOAPODS_VERSION_PATCH_ALAlertBanner 1

